﻿<?php
date_default_timezone_set("Asia/Bangkok");

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/*server*/
$host = '192.168.1.101';
$uname = 'admin';
$passwd = 'nocore';
$database = 'thaikarunafnd';

$today = date("d/m/Y");
$today_apply = date("Y-m-d");
$today_IDX = date("Ymd");

$hostname = $_SERVER['REMOTE_ADDR'];

$pagesize = 20;

function rglob($sDir, $sPattern, $nFlags = NULL)
{
		$sDir = escapeshellcmd($sDir);
	
		$aFiles = glob("$sDir/$sPattern", $nFlags);
	
		foreach (glob("$sDir/*", GLOB_ONLYDIR) as $sSubDir)
		{
			$aSubFiles = rglob($sSubDir, $sPattern, $nFlags);
			$aFiles = array_merge($aFiles, $aSubFiles);
		}
		return $aFiles;
}

$thai_day_arr=array("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์");  

$thai_month_arr=array(  
    "0"=>"",  
    "1"=>"มกราคม",  
    "2"=>"กุมภาพันธ์",  
    "3"=>"มีนาคม",  
    "4"=>"เมษายน",  
    "5"=>"พฤษภาคม",  
    "6"=>"มิถุนายน",   
    "7"=>"กรกฎาคม",  
    "8"=>"สิงหาคม",  
    "9"=>"กันยายน",  
    "10"=>"ตุลาคม",  
    "11"=>"พฤศจิกายน",  
    "12"=>"ธันวาคม"                    
);  

function thai_date($time){  
    global $thai_day_arr,$thai_month_arr;  
    $thai_date_return="วัน".$thai_day_arr[date("w",$time)];  
    $thai_date_return.= "ที่ ".date("j",$time);  
    $thai_date_return.=" ".$thai_month_arr[date("n",$time)];  
    $thai_date_return.= " พ.ศ.".(date("Yํ",$time)+543);  
    #$thai_date_return.= "  ".date("H:i",$time)." น.";  
    return $thai_date_return;  
} 

error_reporting(1);

session_start();

$secret = "karuna".date("d");
$hash = md5($secret);

if ( isset($_SESSION['magic']) && $hash == md5($_SESSION['magic']) ) { 
    echo "<span style='color:darkgreen;background-color:lightgreen;'>อยู่ในระบบ</span> <a href='logout.php'>ออกจากระบบ</a>";
} else {
    echo "<div style='background-color:red;'>";
    echo "Not logged in. Redirect to login page in 3 seconds<br/>";
    echo "</div>";
    echo "<META HTTP-EQUIV='Refresh' CONTENT='3;URL=login.php'>";
} 
?>
