﻿<?php
include('config.php');
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
<HTML xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="th">
	<HEAD>
		<TITLE>THAI KARUNA FOUNDATION</TITLE>
		<LINK rel="stylesheet" type="text/css" href="style.css">
	</HEAD>
	<BODY>
<?php

set_time_limit(0);

if ($_GET[action] == 'medicineReportByQueue'){
	//echo $_GET['action']."<BR>";
	//echo $_GET['date_IDX']."<BR>";
	//echo $_GET['StartQueue']."<BR>";
	//echo $_GET['EndQueue']."<BR>";
	//echo $_GET['Queue_Session']."<BR>";
	
	// Connecting, selecting database
	$link = mysql_connect($host, $uname, $passwd)
		or die('Could not connect: ' . mysql_error());
	mysql_select_db($database) or die('Could not select database');

	mysql_query("SET character_set_results=utf8");
	mysql_query("SET character_set_client=utf8");
	mysql_query("SET character_set_connection=utf8");

	$query = 'SELECT DISTINCT member.MemberID, Title, Name, LastName, medicinetransaction.Transaction_Type, medicinetransaction.Queue, medicinetransaction.Remark, medicinetransaction.Unit, Symptom1, Symptom2, Symptom3, Symptom4, Symptom5, DateApply, DateReallyQualified FROM medicine, medicinetransaction, member, medicineorder WHERE member.memberID = medicineorder.memberID AND member.memberID=medicinetransaction.memberID AND medicine.medicineID=medicineorder.medicineID AND medicinetransaction.Date_IDX=\''.$_GET["date_IDX"].'\' AND medicinetransaction.Queue BETWEEN \''.$_GET['StartQueue'].'\' AND \''.$_GET['EndQueue'].'\' AND medicinetransaction.Queue_Session = \''.$_GET['Queue_Session'].'\' ORDER by DateApply LIMIT 0 , 1000 ';	

	//echo $query;
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());

	// Printing results in HTML
	//echo "ใบจัดยาประจำวันที่ ".$today;
	echo "ใบจัดยาประจำวันที่ ".$_GET['date'];
	$page = 1;
	$seq = 1;
	$count = 1;

	echo "<TABLE CLASS='report' border=1 cellspacing=0 cellpadding=0 width=100% style='font-size:14px;font-weight:200;'>\n";
	echo "\t<TR bgcolor=#666666>";
	echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>ลำดับ</FONT></TD>";
	echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>รหัส</FONT></TD>";
	echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>ชื่อ - นามสกุล</FONT></TD>";
	echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>การขอรับสมุนไพร</FONT></TD>";
	echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>อาการ</FONT></TD>";
	echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>ระยะเวลาที่มารับสมุนไพร</FONT></TD>";
	echo "\t</TR>";
	
	$count = 0;
	
	while ($line = mysql_fetch_row($result)) {
		
		$count++;
		
		$queue = $count;
		if($queue & 1) {
			// Odd number
			echo "\t<TR>\n";
		} else {
			// Even number
			echo "\t<TR bgcolor=#dddddd>\n";
		}

		echo "\t\t<td>".$queue."</td>\n";		
		echo "\t\t<td>".$line[0]."</td>\n";
		echo "\t\t<td>".$line[1]." ".$line[2]." ".$line[3]."</td>\n";
		if ($line[4] == 'TwoWeeks'){
			echo "\t\t<td>สองสัปดาห์</td>\n";		
		} else if ($line[4] == 'Represent') {
			echo "\t\t<td>มีผู้รับแทน</td>\n";
		} else if ($line[4] == 'Represent_TwoWeeks') {
			echo "\t\t<td>มีผู้รับแทน และ สองสัปดาห์</td>\n";
		} else if ($line[4] == 'Double') {
			echo "\t\t<td>สองชุดต่อหนึ่งสัปดาห์</td>\n";
		} else {
			echo "\t\t<td>&nbsp;</td>\n";
		}

		echo "\t\t<td>".$line[8]." ".$line[9]." ".$line[10]." ".$line[11]." ".$line[12]."</td>\n";	

		$date1 = new DateTime(date("Y-m-d"));
		
		if ( $line[13] == "" || $line[13] == "0000-00-00" ){
			$date_apply = $line[14];
		} else {
			$date_apply = $line[13];	
		}
	
		$date2 = new DateTime($date_apply);
		$interval = $date1->diff($date2);

		//echo "\t\t<td>".$date_apply." (" . $interval->y . " ปี " . $interval->m." เดือน ".$interval->d." วัน) </td>\n";	
		//echo "\t\t<td>" . $interval->y . " ปี " . $interval->m." เดือน ".$interval->d." วัน</td>\n";	
		if ( $interval->y > 0 ){
			echo "\t\t<td>".$interval->y." ปี ".$interval->m." เดือน </td>\n";	
		} else if ( $interval->y == 0 && $interval->m > 0 ) {
			echo "\t\t<td>".$interval->m." เดือน </td>\n";		
		} else if ( $interval->m == 0 ){
			echo "\t\t<td>ยังไม่ครบเดือน </td>\n";				
		}
		//echo "\t\t<td>".$line[13]."</td>\n";	

		echo "\t</TR>\n";

	}

	

	//=======================================================



	// Free resultset
	mysql_free_result($result);

	// Closing connection
	mysql_close($link);	

} 

flush();
?>
</BODY>
</HTML>