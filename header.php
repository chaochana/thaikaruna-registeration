﻿	<FONT SIZE=2><?php echo thai_date(strtotime(date('Y-M-d'))) ?></FONT><BR>
	<TABLE border=0 cellpadding=0 cellspacing=0>
	<TR>
		<TD valign=top><IMG src="./img/thaikrnnew.gif"></TD>	
		<TD valign=top><IMG src="./img/blanket_open.gif"></TD>
		<TD>
			<center>
			<A HREF='index.php'>หน้าแรก</A> || <A HREF='member_management.php'>จัดการสมาชิก</A> || <A HREF='medicine_transaction_management_fastq.php'>จัดการระบบคิวรวดเร็ว FastQ&#0174;</A> || <A HREF='medicine_transaction_management_volunteer.php'>จัดการระบบคิวอาสาสมัคร</A> || <A HREF='medicine_transaction_management.php'>จัดการระบบรับสมุนไพร</A>
			</center>
			<UL>			
			<b>รายงาน</b>
				<LI>
				    <FORM METHOD=GET ACTION=medicine_report.php target=blank ID=header>
            			<FONT SIZE=2 COLOR=CC9966>ใบจัดยาตามคิวของวันนี้ เริ่มจากคิวที่</FONT>
            			<INPUT TYPE=HIDDEN NAME=action VALUE='medicineReportByQueue'>
            			<INPUT TYPE=HIDDEN NAME=date_IDX VALUE=<?php echo $today_IDX ?>>
            			<INPUT TYPE=HIDDEN NAME=date VALUE=<?php echo $today ?>>
            			<INPUT CLASS=header TYPE=TEXT NAME=StartQueue SIZE=5><FONT SIZE=2 COLOR=CC9966> =จนถึง=> </FONT>
            			<INPUT CLASS=header TYPE=TEXT NAME=EndQueue SIZE=5><FONT SIZE=2 COLOR=CC996> ของกลุ่ม </FONT>
            			<SELECT CLASS=header NAME='Queue_Session'>
							<OPTION CLASS=header VALUE="Morning">เช้า</OPTION>
							<OPTION CLASS=header VALUE="Afternoon">บ่าย</OPTION>
							<OPTION CLASS=header VALUE="Volunteer1">อาสาสมัคร1</OPTION>
							<OPTION CLASS=header VALUE="Volunteer2">อาสาสมัคร2</OPTION>
							<OPTION CLASS=header VALUE="VIP">กรรมการ</OPTION>
							<OPTION CLASS=header VALUE="Cancer">ผู้ป่วยมะเร็ง</OPTION>	
							<OPTION CLASS=header VALUE="Stroke">อัมพฤก-อัมพาต</OPTION>
							<OPTION CLASS=header VALUE="Chumporn">ชุมพร</OPTION>
							<OPTION CLASS=header VALUE="Military">ทหาร</OPTION>
							</FONT>
						</SELECT>
            			<FONT SIZE=2 COLOR=CC996> แผนก(อาสา) </FONT>
            			<SELECT CLASS=header NAME='Unit'>
            				<OPTION CLASS=header VALUE="all">ทั้งหมด</OPTION>
							<OPTION CLASS=header VALUE="coconut">1.มะพร้าว</OPTION>
							<OPTION CLASS=header VALUE="drinkingwater">2.โรงน้ำดื่มไทยกรุณา</OPTION>
							<OPTION CLASS=header VALUE="stock">3.stock</OPTION>
							<OPTION CLASS=header VALUE="generalpack">4.จัดสมุนไพรทั่วไป</OPTION>
							<OPTION CLASS=header VALUE="garlic">5.กระเทียม</OPTION>
							<OPTION CLASS=header VALUE="rawherb">6.เด็ดใบยา</OPTION>	
							<OPTION CLASS=header VALUE="liquidherb">7.บรรจุยาน้ำ</OPTION>
							<OPTION CLASS=header VALUE="pasteherb">8.ยาลูกกลอน</OPTION>
							<OPTION CLASS=header VALUE="env">9.สิ่งแวดล้อม</OPTION>
							<OPTION CLASS=header VALUE="bottle">10.ล้างขวด</OPTION>
							<OPTION CLASS=header VALUE="kaffirlime">11.มะกรูด</OPTION>
							<OPTION CLASS=header VALUE="bio">12.ชีวภาพ</OPTION>	
							<OPTION CLASS=header VALUE="iravadee">13.อิรวดี</OPTION>
							<OPTION CLASS=header VALUE="uncle">14.ลุงหงำ</OPTION>
							<OPTION CLASS=header VALUE="steam">15.ห้องอบ-ห้องน้ำ</OPTION>
							<OPTION CLASS=header VALUE="construction">16.โยธา</OPTION>
							<OPTION CLASS=header VALUE="traffic">17.จราจร</OPTION>
							<OPTION CLASS=header VALUE="greenherb">18.ยาเขียว</OPTION>
							<OPTION CLASS=header VALUE="registra">19.ทะเบียน</OPTION>							
							<OPTION CLASS=header VALUE="general">20.ทั่วไป</OPTION>														
							<OPTION CLASS=header VALUE="empty">ไม่ได้ระบุ</OPTION>		
							</FONT>
						</SELECT>
            			<INPUT CLASS=header TYPE=SUBMIT VALUE="แสดงใบจัดยา">
            		</FORM>
					</FONT>
				</LI>
				<LI><FONT SIZE=2><A HREF='report.php?date=<?php echo $today_IDX; ?>' TARGET="_blank">รวมยอดใบสั่งสมุนไพรวันนี้</A></FONT></LI>
				<LI><FONT SIZE=2><A HREF='twoweeksreport.php?date=<?php echo $today_IDX; ?>' TARGET="_blank">สมาชิกที่รับสมุนไพรสำหรับสองสัปดาห์ในวันนี้</A></FONT></LI>
				<LI><FONT SIZE=2><A HREF='medicine_transaction_management_volunteer_amount_report.php' TARGET="_blank">สรุปยอดอาสาสมัครทำงาน</A></FONT></LI>					
				<LI><FONT SIZE=2><A HREF='cancer_report.php?action=medicineReportByQueue&date_IDX=<?php echo $today_IDX; ?>&date=<?php echo $today; ?>&StartQueue=1&EndQueue=1000&Queue_Session=Cancer' TARGET="_blank">รายชื่อผู้ป่วยมะเร็งที่มารับสมุนไพรในวันนี้</A></FONT></LI>	
				<LI><FONT SIZE=2><A HREF='cancer_report_by_csvimport.php' TARGET="_blank">รายงายรายละเอียดผู้ป่วยมะเร็งจากCSV</A></FONT></LI>	

			</UL>	

	  </TD>
	  <TD valign=top><IMG src="./img/blanket_close.gif"></TD>
	  <TD valign=top><IMG src="./img/one.gif"></TD>
	</TR>
	</TABLE>
	<HR>