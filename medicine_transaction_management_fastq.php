﻿<?php
include('config.php');
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
<HTML xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<HEAD>
		<TITLE>THAI KARUNA FOUNDATION</TITLE>
		<LINK rel="stylesheet" type="text/css" href="style.css">
		<script type="text/javascript">
			function setFocus(){
				document.FASTQFORM.ID.focus();
			}
		</script>
	</HEAD>
	<body onload="setFocus();">

	<CENTER>
	<A HREF='index.php'> <IMG src="./img/thaikrnnew.gif" width=200></A><BR>
    <FONT SIZE=2><?php echo thai_date(strtotime(date('Y-M-d'))) ?></FONT>
	</CENTER>
<?php

		$link = mysql_connect($host, $uname, $passwd)
			or die('Could not connect: ' . mysql_error());

		mysql_select_db($database) or die('Could not select database');

		mysql_query("SET character_set_results=utf8");
		mysql_query("SET character_set_client=utf8");
		mysql_query("SET character_set_connection=utf8");

		if ( $_GET['ID']==0 || $_GET['ID']==""||!isset($_GET['ID']))
		{
			$LatestQueue_Query = 'SELECT Queue as LatestQueue, Queue_Session FROM medicinetransaction ';
			$LatestQueue_Query = $LatestQueue_Query.' WHERE Date_IDX=\''.$today_IDX.'\' AND SESSION = \''.$hostname.'\'';	
			$LatestQueue_Query = $LatestQueue_Query.' ORDER BY Transaction_ID DESC LIMIT 1';
		
			$LatestQueue_Result = mysql_query($LatestQueue_Query) or die('Query latest queue failed: ' . mysql_error());
			
			$LatestQueue_obj = mysql_fetch_object($LatestQueue_Result);

			$CurrentQueue = $LatestQueue_obj->LatestQueue;
			if ($CurrentQueue == ""){
				$CurrentQueue = 0;
			}
			
			$Queue = $CurrentQueue + 1;
			
			// $LatestSession_Query = 'SELECT Queue_Session FROM medicinetransaction ';
			// $LatestSession_Query = $LatestSession_Query.' WHERE Date_IDX=\''.$today_IDX.'\' AND SESSION = \''.$hostname.'\' ';			
			// $LatestSession_Query = $LatestSession_Query.'AND Queue='.$CurrentQueue;
			// $LatestSession_Query = $LatestSession_Query.' ORDER BY Transaction_ID DESC LIMIT 1';
			
			// $LatestSession_Result = mysql_query($LatestSession_Query) or die('Query last session failed: ' . mysql_error());
			
			// $LatestSession_obj = mysql_fetch_object($LatestSession_Result);
			$Queue_Session = $LatestQueue_obj->Queue_Session;
			
			mysql_free_result($LatestQueue_Result);
			mysql_free_result($LatestSession_Result);
?>

<FORM NAME=FASTQFORM METHOD=GET ACTION=medicine_transaction_management_fastq_confirm.php>
<CENTER>
<DIV style="font-family:'Supermarket';" >
	<TABLE CLASS='fastq' CELLSPACING=0 WIDTH=100%>
		<TR CLASS='fastq'>
			<TD CLASS='fastq'>
				<FONT SIZE=6>SESSION</FONT><BR>
<?php
	echo "<SELECT NAME='Queue_Session' CLASS='fastq' >\n";
	$morning = '<OPTION VALUE="Morning">เช้า</OPTION>';
	$afternoon = '<OPTION VALUE="Afternoon">บ่าย</OPTION>';		
	$volunteer1 = '<OPTION VALUE="Volunteer1">อาสาสมัคร1</OPTION>';		
	$volunteer2 = '<OPTION VALUE="Volunteer2">อาสาสมัคร2</OPTION>';	
	$vip	    = '<OPTION VALUE="VIP">กรรมการ</OPTION>';
	$cancer		= '<OPTION VALUE="Cancer">ผู้ป่วยมะเร็ง</OPTION>';	
	$stroke		= '<OPTION VALUE="Stroke">อัมพฤก-อัมพาต</OPTION>';	
	$chumporn	= '<OPTION VALUE="Chumporn">ชุมพร</OPTION>';	
	$military	= '<OPTION VALUE="Military">ทหาร</OPTION>';	
	switch ($Queue_Session){
		case "Morning":
			$morning = '<OPTION VALUE="Morning" SELECTED>เช้า';
			break;
		case "Afternoon":
			$afternoon = '<OPTION VALUE="Afternoon" SELECTED>บ่าย';
			break;
		case "Volunteer1":
			$volunteer1 = '<OPTION VALUE="Volunteer1" SELECTED>อาสาสมัคร1';
			break;	
		case "Volunteer2":
			$volunteer2 = '<OPTION VALUE="Volunteer2" SELECTED>อาสาสมัคร2';
			break;	
		case "VIP":
			$vip = '<OPTION VALUE="VIP" SELECTED>กรรมการ';
			break;	
		case "Cancer":
			$cancer = '<OPTION VALUE="Cancer" SELECTED>ผู้ป่วยมะเร็ง';
			break;		
		case "Stroke":
			$stroke = '<OPTION VALUE="Stroke" SELECTED>อัมพฤก-อัมพาต';
			break;
		case "Chumporn":
			$cancer = '<OPTION VALUE="Chumporn" SELECTED>ชุมพร';
			break;		
		case "Military":
			$cancer = '<OPTION VALUE="Military" SELECTED>ทหาร';
			break;					
	}
	
	echo $morning."<BR>\n";
	echo $afternoon."<BR>\n";
	echo $volunteer1."<BR>\n";
	echo $volunteer2."<BR>\n";			
	echo $vip."<BR>\n";
	echo $cancer."<BR>\n";	
	echo $stroke."<BR>\n";	
	echo $chumporn."<BR>\n";
	echo $military."<BR>\n";
	echo "</SELECT><BR>\n";
?>				
			</TD>
			<TD CLASS='fastq'>
				<FONT SIZE=6>ประเภทการรับ</FONT><BR>
	<?php
	echo "<SELECT NAME='Transaction_Type' CLASS='fastq'>\n";
	$normal = '<OPTION VALUE="Normal" SELECTED>ปกติ';
	$twoweeks = '<OPTION VALUE="TwoWeeks">สองสัปดาห์';		
	$represent = '<OPTION VALUE="Represent">รับแทน';		
	$represent_twoweeks = '<OPTION VALUE="Represent_TwoWeeks">รับแทน/สองสัปดาห์';		
	$double = '<OPTION VALUE="Double">สองชุดต่อหนึ่งสัปดาห์';	
	
	echo $normal."<BR>\n";
	echo $twoweeks."<BR>\n";
	echo $represent."<BR>\n";
	echo $represent_twoweeks."<BR>\n";
	echo $double."<BR>\n";
			
	echo "</SELECT>";
	?>
			</TD>			
			<TD CLASS='fastq'>
				<FONT SIZE=6>QUEUE</FONT><BR>
				<INPUT TYPE=TEXT SIZE=6 CLASS='fastq' NAME=QUEUE VALUE=<?php echo $Queue?>>
			</TD>
			<TD CLASS='fastq'>
				<FONT SIZE=6>ID</FONT><BR>
				<INPUT TYPE=NUMBER SIZE=1 MAXLENGTH=8 CLASS='fastq' NAME=ID>
			</TD>
		</TR>
		<TR CLASS='fastq'>
			<TD  CLASS='fastq' COLSPAN=4  ALIGN=CENTER><CENTER><INPUT TYPE=SUBMIT VALUE='เพิ่มคิว' SIZE=10 style='font-size:2em'> <INPUT TYPE=RESET VALUE='ยกเลิก' SIZE=10 style='font-size:2em'></CENTER></TD>
		</TR>
	</TABLE>
</DIV>
</CENTER>
</FORM>	

<?php
	} 
?>	
	</BODY>
</HTML>