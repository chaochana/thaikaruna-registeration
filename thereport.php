﻿<?php
include('config.php');
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
<HTML xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<HEAD>
		<TITLE>THAI KARUNA FOUNDATION</TITLE>
		<LINK rel="stylesheet" type="text/css" href="style.css">
		<SCRIPT TYPE="text/javascript">
			<!--
			function popup(mylink, windowname)
			{
			if (! window.focus)return true;
			var href;
			if (typeof(mylink) == 'string')
			   href=mylink;
			else
			   href=mylink.href;
			window.open(href, windowname, 'width=968,height=600,scrollbars=yes');
			return false;
			}
			//-->
		</SCRIPT>
	</HEAD>
	<BODY>
<?php
include("header.php");
?>
	<HR>
<?php
		$link = mysql_connect($host, $uname, $passwd)
			or die('Could not connect: ' . mysql_error());
		// echo 'Connected successfully';

		mysql_select_db($database) or die('Could not select database');

		mysql_query("SET character_set_results=utf8");
		mysql_query("SET character_set_client=utf8");
		mysql_query("SET character_set_connection=utf8");

		// Performing SQL query
		$query = 'SELECT * FROM member';
		
		$result = mysql_query($query) or die('Query failed: ' . mysql_error());

		$member_num_row = mysql_num_rows($result);

		mysql_free_result($result);

		echo "All members: <B>$member_num_row</B><BR>\n";
		
		//$year_array=array(2009,2010,2011);
		$year_array=array(2018,2017,2016);

		foreach ($year_array as $year){
		
			############## ผู้มารับบริการต่อเดือนแบบไม่ซ้ำ
			$sumoftheyear=0;
			for ($month=1;$month<=12;$month++){
				// Performing SQL query
				if ($month >= 1 and $month <= 9) {
					$sqlmonth = "0".$month;
				} else {
					$sqlmonth = $month;
				}
				$query = 'SELECT DISTINCT(member.MemberID) FROM medicineorder, member';
				$query = $query.' WHERE medicineorder.MemberID = member.MemberID';
				$query = $query.' AND medicineorder.Date_IDX like "'.$year.$sqlmonth.'%"';

				$result = mysql_query($query) or die('Query failed: ' . mysql_error());

				$thisyear[$month] = mysql_num_rows($result);
				$sumoftheyear = $sumoftheyear + $thisyear[$month];
			}
			
			echo "<H1>".$year."</H1>";
			echo "<H2>จำนวนผู้มารับบริการต่อเดือนแบบไม่นับซ้ำ</H2>";
			echo "<table border=1 cellpadding=0 cellspacing=0 width=600>";
			echo "<tr><th>เดือน</th><th>จำนวนผู้มาใช้บริการ*</th><th>เปลี่ยนแปลงจากเดือนที่ผ่านมา</th><th>chart (% ของผู้มารับบริการทั้งปี)</th></tr>";		
			for ($month=1;$month<=12;$month++){
			
				$thismonthpercent = ($thisyear[$month]/$sumoftheyear)*100;
				$thismonthpercent = round($thismonthpercent,2);
				$thismonthwidth = $thismonthpercent;
				$previousmonth = $month-1;
				if ($thisyear[$previousmonth] == 0){
					$change_display = "n/a";
				} else {
					$change = $thisyear[$month]/$thisyear[$previousmonth];
					$changepercent = ($change - 1) * 100;
					$changepercent = round($changepercent,2);
					if ($change>1) {
						$change_display = "<font color=green>+".$changepercent."%</font>";
					} else {
						$change_display = "<font color=red>".$changepercent."%</font>";
					}
				}
				
				if ($month >= 1 and $month <= 9) {
					$sqlmonth = "0".$month;
				} else {
					$sqlmonth = $month;
				}
				echo "<tr><td>".$month."/".$year."</td><td>".number_format($thisyear[$month],0,".",",")."</td><td>".$change_display."</td><td><img src=\"./img/".$sqlmonth.".gif\" height=20 width=".$thismonthwidth."%>".$thismonthpercent."%</td></tr>";
			}
			#echo "<tr><td>total</td><td>".$sumoftheyear."</td><td></td><td></td></tr>";
			echo "</table>";			
			echo "* จำนวนผู้รับบริการต่อเดือนแบบไม่นับช้ำ และคิดจากผู้มารับบริการอย่างน้อยหนึ่งครั้งในเดือนน้นๆ";
		######################### สิ้นสุดผู้มารับบริการแบบไม่ซ้ำ #######################
			############## ผู้มารับบริการต่อเดือนแบบนับซ้ำ
			$sumoftheyear=0;
			for ($month=1;$month<=12;$month++){
				// Performing SQL query
				if ($month >= 1 and $month <= 9) {
					$sqlmonth = "0".$month;
				} else {
					$sqlmonth = $month;
				}
				$query = 'SELECT member.MemberID FROM medicinetransaction, member';
				$query = $query.' WHERE medicinetransaction.MemberID = member.MemberID';
				$query = $query.' AND medicinetransaction.Date_IDX like "'.$year.$sqlmonth.'%"';

				$result = mysql_query($query) or die('Query failed: ' . mysql_error());

				$thisyear[$month] = mysql_num_rows($result);
				$sumoftheyear = $sumoftheyear + $thisyear[$month];
			}
			
			echo "<H1>".$year."</H1>";
			echo "<H2>จำนวนผู้มารับบริการต่อเดือนแบบนับซ้ำ</H2>";
			echo "<table border=1 cellpadding=0 cellspacing=0 width=600>";
			echo "<tr><th>เดือน</th><th>จำนวนผู้มาใช้บริการ*</th><th>เปลี่ยนแปลงจากเดือนที่ผ่านมา</th><th>chart (% ของผู้มารับบริการทั้งปี)</th></tr>";		
			for ($month=1;$month<=12;$month++){
			
				$thismonthpercent = ($thisyear[$month]/$sumoftheyear)*100;
				$thismonthpercent = round($thismonthpercent,2);
				$thismonthwidth = $thismonthpercent;
				$previousmonth = $month-1;
				if ($thisyear[$previousmonth] == 0){
					$change_display = "n/a";
				} else {
					$change = $thisyear[$month]/$thisyear[$previousmonth];
					$changepercent = ($change - 1) * 100;
					$changepercent = round($changepercent,2);
					if ($change>1) {
						$change_display = "<font color=green>+".$changepercent."%</font>";
					} else {
						$change_display = "<font color=red>".$changepercent."%</font>";
					}
				}
				
				if ($month >= 1 and $month <= 9) {
					$sqlmonth = "0".$month;
				} else {
					$sqlmonth = $month;
				}
				echo "<tr><td>".$month."/".$year."</td><td>".number_format($thisyear[$month],0,".",",")."</td><td>".$change_display."</td><td><img src=\"./img/".$sqlmonth.".gif\" height=20 width=".$thismonthwidth."%>".$thismonthpercent."%</td></tr>";
			}
			#echo "<tr><td>total</td><td>".$sumoftheyear."</td><td></td><td></td></tr>";
			echo "</table>";			
			echo "* จำนวนผู้รับบริการต่อเดือนแบบไม่นับช้ำ และคิดจากผู้มารับบริการอย่างน้อยหนึ่งครั้งในเดือนน้นๆ";
		######################### สิ้นสุดผู้มารับบริการแบบนับซ้ำ #######################		
		############## สมุนไพรทั้งหมด #############################
			$sumoftheyear=0;
			for ($month=1;$month<=12;$month++){
				// Performing SQL query
				if ($month >= 1 and $month <= 9) {
					$sqlmonth = "0".$month;
				} else {
					$sqlmonth = $month;
				}
				$query = 'SELECT member.MemberID FROM medicineorder, member';
				$query = $query.' WHERE medicineorder.MemberID = member.MemberID';
				$query = $query.' AND medicineorder.Date_IDX like "'.$year.$sqlmonth.'%"';

				$result = mysql_query($query) or die('Query failed: ' . mysql_error());

				$thisyear[$month] = mysql_num_rows($result);
				$sumoftheyear = $sumoftheyear + $thisyear[$month];
			}
			
			echo "<H2>จำนวนสมุนไพร</H2>";
			echo "<table border=1 cellpadding=0 cellspacing=0 width=600>";
			echo "<tr><th>เดือน</th><th>จำนวนสมุนไพร</th><th>เปลี่ยนแปลงจากเดือนที่ผ่านมา</th><th>chart (% ของผู้มารับบริการทั้งปี)</th></tr>";		
			for ($month=1;$month<=12;$month++){
			
				$thismonthpercent = ($thisyear[$month]/$sumoftheyear)*100;
				$thismonthpercent = round($thismonthpercent,2);
				$thismonthwidth = $thismonthpercent;
				$previousmonth = $month-1;
				if ($thisyear[$previousmonth] == 0){
					$change_display = "n/a";
				} else {
					$change = $thisyear[$month]/$thisyear[$previousmonth];
					$changepercent = ($change - 1) * 100;
					$changepercent = round($changepercent,2);
					if ($change>1) {
						$change_display = "<font color=green>+".$changepercent."%</font>";
					} else {
						$change_display = "<font color=red>".$changepercent."%</font>";
					}
				}
				
				if ($month >= 1 and $month <= 9) {
					$sqlmonth = "0".$month;
				} else {
					$sqlmonth = $month;
				}
				echo "<tr><td>".$month."/".$year."</td><td>".number_format($thisyear[$month],0,".",",")."</td><td>".$change_display."</td><td><img src=\"./img/".$sqlmonth.".gif\" height=20 width=".$thismonthwidth."%>".$thismonthpercent."%</td></tr>";
			}
			echo "<tr><td>total</td><td>".number_format($sumoftheyear,0,".",",")."</td><td></td><td></td></tr>";
			echo "</table>";			
		######################### สิ้นสุดสมุนไพรทั้งหมด #######################		
		######################### สมาชิกใหม่ ####################################	
			$sumoftheyear=0;
			for ($month=1;$month<=12;$month++){
				// Performing SQL query
				if ($month >= 1 and $month <= 9) {
					$sqlmonth = "0".$month;
				} else {
					$sqlmonth = $month;
				}
				$query = 'SELECT * FROM member';
				$query = $query.' WHERE DateApply like "'.$year."-".$sqlmonth.'%"';

				$result = mysql_query($query) or die('Query failed: ' . mysql_error());

				$thisyear[$month] = mysql_num_rows($result);
				$sumoftheyear = $sumoftheyear + $thisyear[$month];
			}
			
			echo "<H2>สมาชิกใหม่</H2>";
			echo "<table border=1 cellpadding=0 cellspacing=0 width=600>";
			echo "<tr><th>เดือน</th><th>สมาชิกใหม่</th><th>เปลี่ยนแปลงจากเดือนที่ผ่านมา</th><th>chart (% สมาชิกใหม่)</th></tr>";		
			for ($month=1;$month<=12;$month++){
			
				$thismonthpercent = ($thisyear[$month]/$sumoftheyear)*100;
				$thismonthpercent = round($thismonthpercent,2);
				$thismonthwidth = $thismonthpercent;
				$previousmonth = $month-1;
				if ($thisyear[$previousmonth] == 0){
					$change_display = "n/a";
				} else {
					$change = $thisyear[$month]/$thisyear[$previousmonth];
					$changepercent = ($change - 1) * 100;
					$changepercent = round($changepercent,2);
					
					if ($change>1) {
						$change_display = "<font color=green>+".$changepercent."%</font>";
					} else {
						$change_display = "<font color=red>".$changepercent."%</font>";
					}
				}
				
				if ($month >= 1 and $month <= 9) {
					$sqlmonth = "0".$month;
				} else {
					$sqlmonth = $month;
				}
				echo "<tr><td>".$month."/".$year."</td><td>".number_format($thisyear[$month],0,".",",")."</td><td>".$change_display."</td><td><img src=\"./img/".$sqlmonth.".gif\" height=20 width=".$thismonthwidth."%>".$thismonthpercent."%</td></tr>";
			}
			echo "<tr><td>total</td><td>".number_format($sumoftheyear,0,".",",")."</td><td></td><td></td></tr>";
			echo "</table>";			
		######################### สิ้นสุดสมาชิกใหม่ ################################
		######################### ANNAUL REPORT ###############################
		$query = 'SELECT DISTINCT(member.MemberID) FROM medicineorder, member';
		$query = $query.' WHERE medicineorder.MemberID = member.MemberID';
		$query = $query.' AND medicineorder.Date_IDX like "'.$year.'%"';

		$result = mysql_query($query) or die('Query failed: ' . mysql_error());

		$totalyear = mysql_num_rows($result);
		echo "<h2>Annual Summary</h2>";
		echo "<UL>";
		echo "<LI> We provided herbal medicine for ".$sumoftheyear." times<BR>";
		echo "<LI> We provided herbal medicine to ".$totalyear." people<BR>";	
		echo "</UL>";
		} ## End of foreach Year
		
		

		
		
		

		// Closing connection
		mysql_close($link);
?>

	</BODY>
</HTML>
