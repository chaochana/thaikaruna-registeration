﻿<?php
include('config.php');
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
<HTML xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="th">
	<HEAD>
		<TITLE>THAI KARUNA FOUNDATION</TITLE>
		<LINK rel="stylesheet" type="text/css" href="style.css">
	</HEAD>
	<BODY>
<?php
include("header.php");
?>
	<H1>รายงานประจำวันที่ <?php echo $_GET['date'] ?> เวลา </H1>
<?php

	// Connecting, selecting database
	$link = mysql_connect($host, $uname, $passwd)
		or die('Could not connect: ' . mysql_error());
	mysql_select_db($database) or die('Could not select database');

	mysql_query("SET character_set_results=utf8");
	mysql_query("SET character_set_client=utf8");
	mysql_query("SET character_set_connection=utf8");

	//===== Get New Member ==================================================================

	$query_all_patient = 'SELECT MemberID FROM medicinetransaction WHERE medicinetransaction.DATE_IDX=\''.$today_IDX.'\'';

	$all_patient_result = mysql_query($query_all_patient) or die('Query failed: ' . mysql_error());

	$all_patient = mysql_num_rows($all_patient_result);

	//===== Get Normal Number ==================================================================

	$query_normal_patient = 'SELECT MemberID FROM medicinetransaction WHERE medicinetransaction.DATE_IDX=\''.$today_IDX.'\' AND Transaction_Type LIKE \'Normal\'';

	$normal_patient_result = mysql_query($query_normal_patient) or die('Query failed: ' . mysql_error());

	$normal_patient = mysql_num_rows($normal_patient_result);

	//===== Get TWO WEEKS Number ==================================================================

	$query_twoweeks_patient = 'SELECT * FROM member,medicinetransaction WHERE member.MemberID = medicinetransaction.MemberID AND medicinetransaction.DATE_IDX=\''.$today_IDX.'\' AND Transaction_Type LIKE \'TwoWeeks\'';

	$twoweeks_patient_result = mysql_query($query_twoweeks_patient) or die('Query failed: ' . mysql_error());

	$twoweeks_patient = mysql_num_rows($twoweeks_patient_result);
	
	$twoweeks_patient_result2 = mysql_query($query_twoweeks_patient) or die('Query failed: ' . mysql_error());

	//===== Get Represent Number ==================================================================

	$query_represent_patient = 'SELECT MemberID FROM medicinetransaction WHERE medicinetransaction.DATE_IDX=\''.$today_IDX.'\' AND Transaction_Type LIKE \'Represent\'';

	$represent_patient_result = mysql_query($query_represent_patient) or die('Query failed: ' . mysql_error());

	$represent_patient = mysql_num_rows($represent_patient_result);

	//===== Get Represent Number ==================================================================

	$query_represent_twoweeks_patient = 'SELECT * FROM member,medicinetransaction WHERE member.MemberID = medicinetransaction.MemberID AND medicinetransaction.DATE_IDX=\''.$today_IDX.'\' AND Transaction_Type LIKE \'Represent_TwoWeeks\'';

	$represent_twoweeks_patient_result = mysql_query($query_represent_twoweeks_patient) or die('Query failed: ' . mysql_error());

	$represent_twoweeks_patient = mysql_num_rows($represent_twoweeks_patient_result);

	$represent_twoweeks_patient_result2 = mysql_query($query_represent_twoweeks_patient) or die('Query failed: ' . mysql_error());

	//===== Get Morning Session Number ==================================================================

	$query_morning_patient = 'SELECT MemberID FROM medicinetransaction WHERE medicinetransaction.DATE_IDX=\''.$today_IDX.'\' AND Queue_Session LIKE \'Morning\'';

	$morning_patient_result = mysql_query($query_morning_patient) or die('Query failed: ' . mysql_error());

	$morning_patient = mysql_num_rows($morning_patient_result);

	//===== Get Afternoon Session Number ==================================================================

	$query_afternoon_patient = 'SELECT MemberID FROM medicinetransaction WHERE medicinetransaction.DATE_IDX=\''.$today_IDX.'\' AND Queue_Session LIKE \'Afternoon\'';

	$afternoon_patient_result = mysql_query($query_afternoon_patient) or die('Query failed: ' . mysql_error());

	$afternoon_patient = mysql_num_rows($afternoon_patient_result);
	
	//===== Get Volunteer Session Number ==================================================================

	$query_volunteer_patient = 'SELECT MemberID FROM medicinetransaction WHERE medicinetransaction.DATE_IDX=\''.$today_IDX.'\' AND Queue_Session LIKE \'Volunteer\'';

	$volunteer_patient_result = mysql_query($query_volunteer_patient) or die('Query failed: ' . mysql_error());

	$volunteer_patient = mysql_num_rows($volunteer_patient_result);

	//===== Get New Member ==================================================================

	$query_new_patient = 'SELECT MemberID FROM member WHERE DateApply=\''.$today_apply.'\'';

	$new_patient_result = mysql_query($query_new_patient) or die('Query failed: ' . mysql_error());

	$new_patient = mysql_num_rows($new_patient_result);

	echo "<pre>";
	echo "คนไข้ทั้งหมดของวันนี้: ".$all_patient." ในจำนวนนี้เป็นคนไข้ใหม่(ครบห้าครั้ง เปลี่ยนเป็นครั้งที่หก): ".$new_patient."<BR />";

	echo "ในจำนวนข้างต้นมีผู้ขอสมุนไพร:<BR>";
	echo " - แบ่งตามคิว";
	echo "\t\tคิวเช้า: ".$morning_patient;
	echo "\tคิวบ่าย: ".$afternoon_patient;
	echo "\tคิวอาสาสมัคร: ".$volunteer_patient;
	echo "<HR>";
	echo " - แบ่งตามประเภทการขอรับสมุนไพร";
	echo "\tปกติ: ".$normal_patient;
	echo "\tสองสัปดาห์: ".$twoweeks_patient;
	echo "\tรับยาแทน: ".$represent_patient;
	echo "\tรับยาแทน และ สองสัปดาห์: ".$represent_twoweeks_patient;
	echo "<HR>";
	echo "</pre>";
	
	echo "ผู้รับสมุนไพรสองสัปดาห์";
	echo "<TABLE border=1>\n";
	echo "\t<tr>";
	echo "\t\t<th>หมายเลขสมาชิก</th>";
	echo "\t\t<th>คำนำหน้าชื่อ</th>";
	echo "\t\t<th>ชื่อ</th>";
	echo "\t\t<th>นามสกุล</th>";
	echo "\t\t<th>คิวลำดับที่</th>";
	echo "\t\t<th>กลุ่มคิว</th>";
	echo "\t\t<th>หมายเหตุ</th>";
	echo "\t</tr>";

	while ($row = mysql_fetch_object($twoweeks_patient_result2)) {
		echo "\t<tr>\n";
		echo "\t\t<td>".$row->MemberID."</td>\n";
		echo "\t\t<td>".$row->Title."</td>\n";
		echo "\t\t<td>".$row->Name."</td>\n";
		echo "\t\t<td>".$row->LastName."</td>\n";
		echo "\t\t<td>".$row->Queue."</td>\n";
		echo "\t\t<td>".$row->Queue_Session."</td>\n";
		echo "\t\t<td>".$row->Remark."</td>\n";
		echo "\t</tr>\n";
	}
	echo "</table>\n";


	echo "ผู้รับสมุนไพรแทน และ สองสัปดาห์";
	echo "<TABLE border=1>\n";
	echo "\t<tr>";
	echo "\t\t<th>หมายเลขสมาชิก</th>";
	echo "\t\t<th>คำนำหน้าชื่อ</th>";
	echo "\t\t<th>ชื่อ</th>";
	echo "\t\t<th>นามสกุล</th>";
	echo "\t\t<th>คิวลำดับที่</th>";
	echo "\t\t<th>กลุ่มคิว</th>";
	echo "\t\t<th>หมายเหตุ</th>";
	echo "\t</tr>";

	while ($row = mysql_fetch_object($represent_twoweeks_patient_result2)) {
		echo "\t<tr>\n";
		echo "\t\t<td>".$row->MemberID."</td>\n";
		echo "\t\t<td>".$row->Title."</td>\n";
		echo "\t\t<td>".$row->Name."</td>\n";
		echo "\t\t<td>".$row->LastName."</td>\n";
		echo "\t\t<td>".$row->Queue."</td>\n";
		echo "\t\t<td>".$row->Queue_Session."</td>\n";
		echo "\t\t<td>".$row->Remark."</td>\n";
		echo "\t</tr>\n";
	}
	echo "</table>\n";
	// Closing connection
	mysql_close($link);
?>
</BODY>
</HTML>