<?php
    session_start();
    session_unset();
    session_destroy();
    session_write_close();
    setcookie(session_name(),'',0,'/');
    ini_set('session.gc_max_lifetime', 0);
    ini_set('session.gc_probability', 1);
    ini_set('session.gc_divisor', 1);
    session_regenerate_id(true);
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
ออกจากระบบแล้ว
<br/>
<a href='login.php'>ไปหน้าเข้าสู่ระบบ</a>
<br/>
พาคุณไปหน้าเข้าสู่ระบบอัตโนมัติใน 3 วินาที...<br/>
<META HTTP-EQUIV='Refresh' CONTENT='3;URL=login.php'>