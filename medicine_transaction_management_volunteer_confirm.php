﻿<?php
include('config.php');
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
<HTML xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<HEAD>
		<TITLE>THAI KARUNA FOUNDATION</TITLE>
		<LINK rel="stylesheet" type="text/css" href="style.css">
	</HEAD>
	<BODY onLoad="document.FASTQFORM.ID.focus()">
<?php
//include("header_mini.php");
?>
	<CENTER>
	<A HREF='index.php'> <IMG src="./img/thaikrnnew.gif" width=200></A><BR>
    <FONT SIZE=2><?php echo thai_date(strtotime(date('Y-M-d'))) ?></FONT>
	</CENTER>

	<?php

		$link = mysql_connect($host, $uname, $passwd)
			or die('Could not connect: ' . mysql_error());
		// echo 'Connected successfully';

		mysql_select_db($database) or die('Could not select database');

		mysql_query("SET character_set_results=utf8");
		mysql_query("SET character_set_client=utf8");
		mysql_query("SET character_set_connection=utf8");


	if ( $_GET['ID'] == 0 || $_GET['ID'] == "" ) // No ID, shows error
	{
	?>
		<center>
			<h1>ไม่ได้ระบุรหัสสมาชิก</h1>
			<form ACTION='medicine_transaction_management_volunteer.php'>			
				<table>
					<tr>
						<td  CLASS='fastq-green' ALIGN=RIGHT>
							<button type=submit style='font-size:2em'>
								<img src="./img/red.jpg" alt="Cancel" width=30/>
								<font SIZE=5>ยกเลิก</font>
							</button>
						</td>
					</tr>
				</table>
			</form>	
		</center>
	<?php
	}
	else if ( $_GET['Unit'] == "" ) // No Unit, shows error
	{
	?>
		<center>
			<h1>ไม่ได้ระบุแผนก</h1>
			<form ACTION='medicine_transaction_management_volunteer.php'>			
				<table>
					<tr>
						<td  CLASS='fastq-green' ALIGN=RIGHT>
							<button type=submit style='font-size:2em'>
								<img src="./img/red.jpg" alt="Cancel" width=30/>
								<font SIZE=5>ยกเลิก</font>
							</button>
						</td>
					</tr>
				</table>
			</form>	
		</center>

	<?php
	} else {
	?>
<FORM NAME=FASTQCONFIRM METHOD=GET ACTION='medicine_transaction_management_volunteer_add.php'>
<CENTER>
<DIV style="font-family:'Supermarket';font-size:24px;" >
	<TABLE CLASS='fastq-green' CELLSPACING=0>
		<TR CLASS='fastq-green'>
			<TD CLASS='fastq-green'>
				<FONT STYLE="font-size:36px"><B>ประเภทคิว</B></FONT><BR>
				<FONT STYLE="font-size:48px">
<?php
				echo "อาสา - ";
				switch ( $_GET['Unit'] )
				{
					case "coconut":			echo "1.มะพร้าว"; 		break;
					case "drinkingwater":	echo "2.โรงน้ำดื่มไทยกรุณา";	break;
					case "stock":			echo "3.stock";		break;	
					case "generalpack":		echo "4.จัดสมุนไพรทั่วไป";	break;	
					case "garlic":			echo "5.กระเทียม";		break;	
					case "rawherb":			echo "6.เด็ดใบยา";		break;		
					case "liquidherb":		echo "7.บรรจุยาน้ำ";		break;		
					case "pasteherb":		echo "8.ยาลูกกลอน";		break;
					case "env":				echo "9.สิ่งแวดล้อม";		break;
					case "bottle":			echo "10.ล้างขวด";		break;	
					case "kaffirlime":		echo "11.มะกรูด";		break;		
					case "bio":				echo "12.ชีวภาพ";		break;		
					case "iravadee":		echo "13.อิรวดี";		break;		
					case "uncle":			echo "14.น้าหงำ";		break;	
					case "steam":			echo "15.ห้องอบ-ห้องน้ำ";	break;										
					case "construction":	echo "16.โยธา";		break;
					case "traffic":			echo "17.จราจร";		break;
					case "greenherb":		echo "18.ยาเขียว";		break;
					case "registra":		echo "19.ทะเบียน";		break;
					case "general":			echo "20.ทั่วไป";		break;					
					case "":				echo "ไม่ได้ระบุ";		break;
				}
				
				echo "<INPUT TYPE=HIDDEN NAME='Queue_Session' VALUE='".$_GET['Queue_Session']."'>";
				echo "<INPUT TYPE=HIDDEN NAME='Unit' VALUE='".$_GET['Unit']."'>";
?>				
			</FONT>
			</TD>
			<TD CLASS='fastq-green'>
				<FONT STYLE="font-size:36px"><B>ประเภทการรับ</B></FONT><BR>
				<FONT STYLE="font-size:60px">
				<?php
				switch ($_GET['Transaction_Type'])
				{
					case "Normal":		echo "ปกติ";		break;
					case "TwoWeeks":	echo "สองสัปดาห์";	break;
					case "Represent":	echo "รับแทน";		break;			
					case "Represent_TwoWeeks" :	echo "รับแทน/สองสัปดาห์";	break;		
					case "Double":		echo "สองชุดต่อหนึ่งสัปดาห์";			break;				
				}
				
				echo "<INPUT TYPE=HIDDEN NAME='Transaction_Type' VALUE='".$_GET['Transaction_Type']."'>";
				?>
				</FONT>
			</TD>			
			<TD CLASS='fastq-green'>
				<FONT STYLE="font-size:36px"><B>คิว</B></FONT><BR>
				<FONT STYLE="font-size:60px"><?php echo $_GET['QUEUE']?></FONT>
				<INPUT TYPE=HIDDEN SIZE=6 CLASS='fastq' NAME=QUEUE VALUE=<?php echo $_GET['QUEUE']?> >
			</TD>
			<TD CLASS='fastq-green'>
				<FONT STYLE="font-size:36px"><B>รหัส</B></FONT><BR>
				<FONT STYLE="font-size:60px"><?php echo $_GET['ID']?></FONT>
				<INPUT TYPE=HIDDEN SIZE=1 MAXLENGTH=8 CLASS='fastq' NAME=ID VALUE=<?php echo $_GET['ID']?> >
			</TD>
		</TR>
		<TR CLASS='fastq-lightgreen'>
			<TD  CLASS='fastq-lightgreen' COLSPAN=4  ALIGN=CENTER BACKGROUND=YELLOW>
			<CENTER>
<?php
//////////////////////// แสดงข้อมูลก่อนทำการเพิ่มคิว			

			$query = 'SELECT * FROM member WHERE MemberID = '.$_GET['ID'];
			
			$result = mysql_query($query) or die('Query failed: ' . mysql_error());
		
			$num_row = mysql_num_rows($result);

			if ( $num_row == 0)	echo "<FONT SIZE=4 COLOR=RED>ไม่มีข้อมูลตามคำค้น / ไม่พบรหัสสมาชิกนี้</FONT><BR>"; // No Member ID in Database
			else if ( $num_row = 1 ) { // MemberID found
				$row = mysql_fetch_object($result);
				
				// Show Image and Name
				$img="./img/face/".$row->MemberID.".jpg";
				if ( file_exists($img) ) echo "<IMG WIDTH=100 SRC=\"./img/face/".$row->MemberID.".jpg\" width=150>";
				else echo "<BR>";
				echo "<FONT style=\"font-size:60px;\">".$row->Title." ".$row->Name." ".$row->LastName." [".$row->MemberID."]</FONT><BR>";

				$Note=$row->Note;
				echo "<FONT size=5 style='color:red'>หมายเหตุ: </FONT>";
				echo "<FONT size=5 style='color:red'><b>".$row->Note."</b></FONT><BR>";
				
				if (preg_match("/\*ALERT\*/", $Note)) {
					echo '<script type="text/javascript">alert("'.str_replace(array("\n","\r","*ALERT*"), " ", $Note).'"); </script>';
				}	

				// Last medication pickup date
				$query = 'SELECT Max(Date_IDX) as MaxOrderDate_IDX FROM medicinetransaction WHERE MemberID='.$row->MemberID;
				$result = mysql_query($query) or die('Query failed: ' . mysql_error());
				$row2 = mysql_fetch_object($result);
				$lastest_date = $row2->MaxOrderDate_IDX;
				
				if ( $lastest_date != "" ) 
				{
					echo "<font size=5>รับสมุนไพรครั้งล่าสุด ".thai_date(strtotime(substr($lastest_date,6,2).'-'.substr($lastest_date,4,2).'-'.substr($lastest_date,0,4)))."</font><BR><BR>";
					$flag_error = "no";
				} else {
					echo "<font color=red  size=5><b>ไม่พบข้อมูลการรับสมุนไพร</b></font>";
					$flag_error = "yes";
				}
				mysql_free_result($result);
///////////////////////////  สมุนไพรวันนี้

	$transaction_query = 'SELECT * ';
	$transaction_query = $transaction_query.'FROM medicinetransaction ';
	$transaction_query = $transaction_query.'WHERE MemberID='.$row->MemberID.' AND Date_IDX=\''.$today_IDX.'\'';

	$result_transaction = mysql_query($transaction_query) or die('Query failed: ' . mysql_error());
	
	$obj = mysql_fetch_object($result_transaction);

	if (isset($obj->Transaction_ID) && $obj->Transaction_ID != "") {
		echo "<B><font color=red size=5>มีการบันทึกการรับสมุนไพรของวันนี้แล้ว</font></b><BR>";
		
		$Transaction_ID = $obj->Transaction_ID;
		$Queue = $obj->Queue;
		$Transaction_Type = $obj->Transaction_Type;
		$Remark = $obj->Remark;
		
		switch ( $obj->Queue_Session )
		{
			case "Morning":		$Queue_Session = "เช้า";	break;
			case "Afternoon":	$Queue_Session = "บ่าย";	break;
			case "Volunteer1":	$Queue_Session = "อาสาสมัคร1";	break;	
			case "Volunteer2":	$Queue_Session = "อาสาสมัคร2";	break;	
			case "VIP":			$Queue_Session = "กรรมการ";	break;	
			case "Cancer":		$Queue_Session = "ผู้ป่วยมะเร็ง";	break;		
			case "Stroke":		$Queue_Session = "อัมพฤก-อัมพาต";	break;				
			case "Chumporn":	$Queue_Session = "ชุมพร";		break;				
			case "Military":	$Queue_Session = "ทหาร";		break;								
		}
		
		switch ( $Transaction_Type )
		{
			case "Normal":		$transaction_type_display = "ปกติ";		break;
			case "TwoWeeks":	$transaction_type_display = "สองสัปดาห์";	break;
			case "Represent":	$transaction_type_display = "รับแทน";		break;			
			case "Represent_TwoWeeks" :	$transaction_type_display = "รับแทน/สองสัปดาห์";	break;		
			case "Double":		$transaction_type_display = "สองชุดต่อหนึ่งสัปดาห์";			break;				
		}
		
		echo "<font color=red size=5>คิวประเภท: <B> ${Queue_Session} </B>คิว: <B> ${Queue} </B>ประเภทการรับ: <b> ${transaction_type_display} </b></font><BR><BR>";
		echo "<B><font color=red size=5>กดยืนยันเพื่อแก้คิว</font></b><BR>";
	}

////////////////////////// รับยาพิเศษครั้งสุดท้าย

	$Last_Transaction_Type_Query = 'SELECT * FROM medicinetransaction ';
	$Last_Transaction_Type_Query = $Last_Transaction_Type_Query.'WHERE MemberID=\''.$row->MemberID.'\' AND ';
	$Last_Transaction_Type_Query = $Last_Transaction_Type_Query.'DATE_IDX=\''.$lastest_date.'\'';
	//$lastest_date
	
	//echo $Last_Transaction_Type_Query;

	$Last_Transaction_Type_Result = mysql_query($Last_Transaction_Type_Query) or die('Query last transaction type failed: ' . mysql_error());
		
	$Latest_Transaction_Type_obj = mysql_fetch_object($Last_Transaction_Type_Result);
	$Transaction_Type = $Latest_Transaction_Type_obj->Transaction_Type;
	if(isset($Lastest_Transaction_Type_obj->Remark)){
		$Remark = $Lastest_Transaction_Type_obj->Remark;
	} else {
		$Remark = "";
	}
	
	if ($Transaction_Type == "TwoWeeks"){
		//echo "<font color=red><b>คนไข้รับสมุนไพรสำหรับสองสัปดาห์ครั้งล่าสุดเมื่อวันที่ (ปี/เดือน/วัน) ".$lastest_date." โดยมีเหตุ: ".$Remark."</b></font>";
		//[Fixed date to readable format by Chao 20130302]
		echo "<font color=red size=5><b>คนไข้รับสมุนไพรสำหรับสองสัปดาห์ครั้งล่าสุดเมื่อ ".thai_date(strtotime($lastest_date))." โดยมีเหตุ: ".$Remark."</b></font>";
	} else if ($Transaction_Type == "Represent"){
		//echo "<font color=red><b>คนไข้มีผู้มารับสมุนไพรแทน ครั้งสุดท้ายเมื่อวันที่ ".$lastest_date." โดยมีเหตุ: ".$Remark."</b></font>";
		//[Fixed date to readable format by Chao 20130302]
		echo "<font color=red size=5><b>คนไข้มีผู้มารับสมุนไพรแทน ครั้งล่าสุดเมื่อ ".thai_date(strtotime($lastest_date))." โดยมีเหตุ: ".$Remark."</b></font>";
	} else if ($Transaction_Type == "Double"){
		echo "<font color=red size=5><b>คนไข้รับสมุนไพรสองชุดต่อหนึ่งสัปดาห์ ครั้งล่าสุดเมื่อ ".thai_date(strtotime($lastest_date))." โดยมีเหตุ: ".$Remark."</b></font>";
	} else if ($Transaction_Type == "Represent_TwoWeeks"){
		echo "<font color=red size=5><b>คนไข้มีผู้มารับสมุนไพรแทนสองสัปดาห์ ครั้งล่าสุดเมื่อ ".thai_date(strtotime($lastest_date))." โดยมีเหตุ: ".$Remark."</b></font>";
	}						
			
			}
			
/*       แสดงการรับสมุนไพรย้อนหลังสามครั้ง        */			
	$query = 'SELECT DATE_IDX FROM medicineorder WHERE MemberID = '.$row->MemberID.' GROUP BY DATE_IDX ORDER BY DATE_IDX DESC LIMIT 3';
	

	$result = mysql_query($query) or die('Query failed: ' . mysql_error());

	//$num_row = mysql_num_rows($result);
    echo "<H2>แสดงการรับสมุนไพรย้อนหลัง 3 สัปดาห์ล่าสุด</H2>\n";
	echo "<TABLE border=1>\n";
	echo "\t<tr>";
	echo "\t\t<th bgcolor=CDCDCD>วันที่มารับสมุนไพร</th>";
	echo "\t\t<Th align=center valign=top><FONT SIZE='' COLOR='#000000'>คิว</FONT></Th>";	
	echo "\t\t<Th align=center valign=top><FONT SIZE='' COLOR='#000000'>การขอรับสมุนไพร</FONT></Th>";
	echo "\t\t<th align=center valign=top><FONT SIZE='' COLOR='#000000'>001<BR>เขียว</FONT></th>";
	echo "\t\t<th align=center valign=top><FONT SIZE='' COLOR='#000000'>002<BR>ฟอก</FONT></th>";
	echo "\t\t<th align=center valign=top><FONT SIZE='' COLOR='#000000'>003<BR>ปอด</FONT></th>";
	echo "\t\t<th align=center valign=top><FONT SIZE='' COLOR='#000000'>004<BR>เบาหวาน</FONT></th>";
	echo "\t\t<th align=center valign=top><FONT SIZE='' COLOR='#000000'>005<BR>มะกรูด</FONT></th>";
	echo "\t\t<th align=center valign=top><FONT SIZE='' COLOR='#000000'>006<BR>น้ำผึ้ง</FONT></th>";
	echo "\t\t<th align=center valign=top><FONT SIZE='' COLOR='#000000'>007<BR>มะนาว</FONT></th>";
	echo "\t\t<th align=center valign=top><FONT SIZE='' COLOR='#000000'>008<BR>กระดูกหมู</FONT></th>";
	echo "\t\t<th align=center valign=top><FONT SIZE='' COLOR='#000000'>009<BR>มะพร้าว</FONT></th>";
	echo "\t\t<th align=center valign=top><FONT SIZE='' COLOR='#000000'>010<BR>ตำลึง</FONT></th>";
	echo "\t\t<th align=center valign=top><FONT SIZE='' COLOR='#000000'>011<BR>หยอดตา</FONT></th>";
	echo "\t\t<th align=center valign=top><FONT SIZE='' COLOR='#000000'>012<BR>ตัด</FONT></th>";
	echo "\t\t<th align=center valign=top><FONT SIZE='' COLOR='#000000'>013<BR>ดูดหนอง</FONT></th>";
	echo "\t\t<th align=center valign=top><FONT SIZE='' COLOR='#000000'>014<BR>ไพลประคบ</FONT></th>";
	echo "\t\t<th align=center valign=top><FONT SIZE='' COLOR='#000000'>015<BR>ไพลน้ำ</FONT></th>";
	echo "\t\t<th align=center valign=top><FONT SIZE='' COLOR='#000000'>016<BR>ทาผิว</FONT></th>";
	echo "\t\t<th align=center valign=top><FONT SIZE='' COLOR='#000000'>017<BR>ระบาย</FONT></th>";	
	echo "\t\t<th align=center valign=top><FONT SIZE='' COLOR='#000000'>018<BR>รมตา</FONT></th>";	

	echo "\t</tr>";
	
	while ($rowx = mysql_fetch_object($result)) {
		$order_date = $rowx->DATE_IDX;
		$order_date = substr($order_date,6,2).'-'.substr($order_date,4,2).'-'.substr($order_date,0,4);
		
		$query2 = 'SELECT medicineorder.medicineID, medicineName, Amount, Add_Amount, Special FROM medicine, medicineorder, member WHERE member.memberID=medicineorder.memberID AND medicine.medicineID=medicineorder.medicineID AND medicineorder.Date_IDX=\''.$rowx->DATE_IDX.'\' AND member.MemberID=\''.$row->MemberID.'\' LIMIT 0 , 30';

		//echo $query2;
		$result2 = mysql_query($query2) or die('Query failed: ' . mysql_error());

		$medicine001 = "&nbsp;";
		$medicine002 = "&nbsp;";
		$medicine003 = "&nbsp;";
		$medicine004 = "&nbsp;";
		$medicine005 = "&nbsp;";
		$medicine006 = "&nbsp;";
		$medicine007 = "&nbsp;";
		$medicine008 = "&nbsp;";
		$medicine009 = "&nbsp;";
		$medicine010 = "&nbsp;";
		$medicine011 = "&nbsp;";
		$medicine012 = "&nbsp;";
		$medicine013 = "&nbsp;";
		$medicine014 = "&nbsp;";
		$medicine015 = "&nbsp;";
		$medicine016 = "&nbsp;";
		$medicine017 = "&nbsp;";
		$medicine018 = "&nbsp;";
		
		$add_medicine001 = "&nbsp;";
		$add_medicine002 = "&nbsp;";
		$add_medicine003 = "&nbsp;";
		$add_medicine004 = "&nbsp;";
		$add_medicine005 = "&nbsp;";
		$add_medicine006 = "&nbsp;";
		$add_medicine007 = "&nbsp;";
		$add_medicine008 = "&nbsp;";
		$add_medicine009 = "&nbsp;";
		$add_medicine010 = "&nbsp;";
		$add_medicine011 = "&nbsp;";
		$add_medicine012 = "&nbsp;";		
		$add_medicine013 = "&nbsp;";
		$add_medicine014 = "&nbsp;";
		$add_medicine015 = "&nbsp;";
		$add_medicine016 = "&nbsp;";
		$add_medicine017 = "&nbsp;";	
		$add_medicine018 = "&nbsp;";
		
		while ($row_medicine = mysql_fetch_object($result2))
		{
			//$total = $row_medicine->Amount + $row_medicine->Add_Amount;

			switch ($row_medicine->medicineID) 
			{
				case '001':
					$medicine001 = $row_medicine->Amount;
					$add_medicine001 = $row_medicine->Add_Amount;
					break;
				case '002':
					$medicine002 = $row_medicine->Amount;
					$add_medicine002 = $row_medicine->Add_Amount;
					break;
				case '003':
					$medicine003 = $row_medicine->Amount;
					$add_medicine003 = $row_medicine->Add_Amount;
					break;
				case '004':
					$medicine004 = $row_medicine->Amount;
					$add_medicine004 = $row_medicine->Add_Amount;
					break;
				case '005':
					$medicine005 = $row_medicine->Amount;
					$add_medicine005 = $row_medicine->Add_Amount;
					break;
				case '006':
					$medicine006 = $row_medicine->Amount;
					$add_medicine006 = $row_medicine->Add_Amount;
					break;
				case '007':
					$medicine007 = $row_medicine->Amount;
					$add_medicine007 = $row_medicine->Add_Amount;
					break;
				case '008':
					$medicine008 = $row_medicine->Amount;
					$add_medicine008 = $row_medicine->Add_Amount;
					break;
				case '009':
					$medicine009 = $row_medicine->Amount;
					$add_medicine009 = $row_medicine->Add_Amount;
					break;
				case '010':
					$medicine010 = $row_medicine->Amount;
					$add_medicine010 = $row_medicine->Add_Amount;
					break;
				case '011':
					$medicine011 = $row_medicine->Amount;
					$add_medicine011 = $row_medicine->Add_Amount;
					break;
				case '012':
					$medicine012 = $row_medicine->Amount;
					$add_medicine012 = $row_medicine->Add_Amount;
					break;
				case '013':
					$medicine013 = $row_medicine->Amount;
					$add_medicine013 = $row_medicine->Add_Amount;
					break;			
				case '014':
					$medicine014 = $row_medicine->Amount;
					$add_medicine014 = $row_medicine->Add_Amount;
					break;			
				case '015':
					$medicine015 = $row_medicine->Amount;
					$add_medicine015 = $row_medicine->Add_Amount;
					break;			
				case '016':
					$medicine016 = $row_medicine->Amount;
					$add_medicine016 = $row_medicine->Add_Amount;
					break;			
				case '017':
					$medicine017 = $row_medicine->Amount;
					$add_medicine017 = $row_medicine->Add_Amount;
					break;		
				case '018':
					$medicine018 = $row_medicine->Amount;
					$add_medicine018 = $row_medicine->Add_Amount;
					break;						
			}
			
			switch ($row_medicine->Special)
			{
	    		case 'เขียวเต็ม':
				$special_medicine001 = " เต็ม";
				break;
			case 'ปอดต้มเอง':
				$special_medicine003 = " ต้มเอง";
				break;
	    		case 'เบาหวานต้มเอง':
				$special_medicine004 = " ต้มเอง";
				break;
			case 'ดำพิเศษ':
				$special_medicine006 = " พิ";
				break;
			case 'เข้มข้น':
				$special_medicine008 = " ข้น";
				break;
	    		case 'มะพร้าวต้มเอง':
				$special_medicine009 = " ต้มเอง";
				break;
			case 'ตำลึงเต็ม':
				$special_medicine010 = " เต็ม";
				break;
			}
		}

		echo "\t<tr>\n";
		echo "\t\t<td>".thai_date(strtotime($order_date))."</td>\n";

		$query_transaction_type = 'SELECT medicinetransaction.Queue_Session, medicinetransaction.Unit, medicinetransaction.Transaction_Type, medicinetransaction.Remark FROM medicine, medicinetransaction, member, medicineorder WHERE member.memberID = medicineorder.memberID AND member.memberID=medicinetransaction.memberID AND medicine.medicineID=medicineorder.medicineID AND medicinetransaction.Date_IDX=\''.$rowx->DATE_IDX.'\' AND member.MemberID=\''.$row->MemberID.'\'';
		
		//echo $query_transaction_type;
		
		$result_transaction_type = mysql_query($query_transaction_type) or die('Query failed: ' . mysql_error());

		$obj_transaction_type = mysql_fetch_object($result_transaction_type);


		echo "\t\t<td>";

		switch ($obj_transaction_type->Queue_Session){
			case "Morning":
				echo "เช้า";
				break;
			case "Afternoon":
				echo "บ่าย";
				break;
			case "Volunteer1":
				echo "อาสาสมัคร1";
				break;	
			case "Volunteer2":
				echo "อาสาสมัคร2";
				break;	
			case "VIP":
				echo "กรรมการ";
				break;	
			case "Cancer":
				echo "ผู้ป่วยมะเร็ง";
				break;		
			case "Stroke":
				echo "อัมพฤก-อัมพาต";
				break;	
			case "Chumporn":
				echo "ชุมพร";
				break;				
			case "Military":
				echo "ทหาร";
				break;								
		}
		if($obj_transaction_type->Queue_Session == "Volunteer1" || $obj_transaction_type->Queue_Session == "Volunteer2"){
			echo " - ";
			switch ($obj_transaction_type->Unit){
				case "coconut":
					echo "1.มะพร้าว";
					break;
				case "drinkingwater":
					echo "2.โรงน้ำดื่มไทยกรุณา";	
					break;
				case "stock":
					echo "3.stock";
					break;	
				case "generalpack":
					echo "4.จัดสมุนไพรทั่วไป";	
					break;	
				case "garlic":
					echo "5.กระเทียม";
					break;	
				case "rawherb":
					echo "6.เด็ดใบยา";
					break;		
				case "liquidherb":
					echo "7.บรรจุยาน้ำ";
					break;		
				case "pasteherb":
					echo "8.ยาลูกกลอน";
					break;			
				case "env":
					echo "9.สิ่งแวดล้อม";
					break;	
				case "bottle":
					echo "10.ล้างขวด";
					break;	
				case "kaffirlime":
					echo "11.มะกรูด";
					break;		
				case "bio":
					echo "12.ชีวภาพ";
					break;		
				case "iravadee":
					echo "13.อิรวดี";
					break;		
				case "uncle":
					echo "14.น้าหงำ";
					break;	
				case "steam":
					echo "15.ห้องอบ-ห้องน้ำ";
					break;										
				case "construction":
					echo "16.โยธา";
					break;
				case "traffic":
					echo "17.จราจร";
					break;
				case "greenherb":
					echo "18.ยาเขียว";
					break;
				case "registra":
					echo "19.ทะเบียน";
					break;
				case "general":
					echo "20.ทั่วไป";
					break;					
				case "":
					echo "ไม่ได้ระบุ";
					break;
					}
		}
		echo "</td>\n";

		if ($obj_transaction_type->Transaction_Type == 'TwoWeeks'){
			echo "\t\t<td>สองสัปดาห์</td>\n";		
		} else if ($obj_transaction_type->Transaction_Type == 'Represent') {
			echo "\t\t<td>มีผู้รับแทน</td>\n";
		} else if ($obj_transaction_type->Transaction_Type == 'Represent_TwoWeeks') {
			echo "\t\t<td>มีผู้รับแทน และ สองสัปดาห์</td>\n";
		} else if ($obj_transaction_type->Transaction_Type == 'Double') {
			echo "\t\t<td>สองชุดต่อหนึ่งสัปดาห์</td>\n";
		} else {
			echo "\t\t<td>&nbsp;</td>\n";
		}
		
		if ($add_medicine001+$medicine001>0) {  $color001='color=red'; }
		if ($add_medicine002+$medicine002>0) {  $color002='color=red'; }
		if ($add_medicine003+$medicine003>0) {  $color003='color=red'; }
		if ($add_medicine004+$medicine004>0) {  $color004='color=red'; }		
		if ($add_medicine005+$medicine005>0) {  $color005='color=red'; }
		if ($add_medicine006+$medicine006>0) {  $color006='color=red'; }
		if ($add_medicine007+$medicine007>0) {  $color007='color=red'; }
		if ($add_medicine008+$medicine008>0) {  $color008='color=red'; }				
		if ($add_medicine009+$medicine009>0) {  $color009='color=red'; }
		if ($add_medicine010+$medicine010>0) {  $color010='color=red'; }
		if ($add_medicine011+$medicine011>0) {  $color011='color=red'; }
		if ($add_medicine012+$medicine012>0) {  $color012='color=red'; }		
		if ($add_medicine013+$medicine013>0) {  $color013='color=red'; }				
		if ($add_medicine014+$medicine014>0) {  $color014='color=red'; }
		if ($add_medicine015+$medicine015>0) {  $color015='color=red'; }
		if ($add_medicine016+$medicine016>0) {  $color016='color=red'; }
		if ($add_medicine017+$medicine017>0) {  $color017='color=red'; }				
		if ($add_medicine018+$medicine018>0) {  $color018='color=red'; }						
		
		echo "\t\t<td align=center valign=top><FONT SIZE=4  ".$color001."><B>".($add_medicine001+$medicine001).$special_medicine001."</B></FONT><BR><FONT SIZE=2>".$medicine001."+(".$add_medicine001.")</FONT></td>\n";
		echo "\t\t<td align=center valign=top><FONT SIZE=4  ".$color002."><B>".($add_medicine002+$medicine002).$special_medicine002."</B></FONT><BR><FONT SIZE=2>".$medicine002."+(".$add_medicine002.")</FONT></td>\n";		
		echo "\t\t<td align=center valign=top><FONT SIZE=4  ".$color003."><B>".($add_medicine003+$medicine003).$special_medicine003."</B></FONT><BR><FONT SIZE=2>".$medicine003."+(".$add_medicine003.")</FONT></td>\n";		
		echo "\t\t<td align=center valign=top><FONT SIZE=4  ".$color004."><B>".($add_medicine004+$medicine004).$special_medicine004."</B></FONT><BR><FONT SIZE=2>".$medicine004."+(".$add_medicine004.")</FONT></td>\n";		
		echo "\t\t<td align=center valign=top><FONT SIZE=4  ".$color005."><B>".($add_medicine005+$medicine005).$special_medicine005."</B></FONT><BR><FONT SIZE=2>".$medicine005."+(".$add_medicine005.")</FONT></td>\n";
		echo "\t\t<td align=center valign=top><FONT SIZE=4  ".$color006."><B>".($add_medicine006+$medicine006).$special_medicine006."</B></FONT><BR><FONT SIZE=2>".$medicine006."+(".$add_medicine006.")</FONT></td>\n";		
		echo "\t\t<td align=center valign=top><FONT SIZE=4  ".$color007."><B>".($add_medicine007+$medicine007).$special_medicine007."</B></FONT><BR><FONT SIZE=2>".$medicine007."+(".$add_medicine007.")</FONT></td>\n";		
		echo "\t\t<td align=center valign=top><FONT SIZE=4  ".$color008."><B>".($add_medicine008+$medicine008).$special_medicine008."</B></FONT><BR><FONT SIZE=2>".$medicine008."+(".$add_medicine008.")</FONT></td>\n";			
		echo "\t\t<td align=center valign=top><FONT SIZE=4  ".$color009."><B>".($add_medicine009+$medicine009).$special_medicine009."</B></FONT><BR><FONT SIZE=2>".$medicine009."+(".$add_medicine009.")</FONT></td>\n";
		echo "\t\t<td align=center valign=top><FONT SIZE=4  ".$color010."><B>".($add_medicine010+$medicine010).$special_medicine010."</B></FONT><BR><FONT SIZE=2>".$medicine010."+(".$add_medicine010.")</FONT></td>\n";		
		echo "\t\t<td align=center valign=top><FONT SIZE=4  ".$color011."><B>".($add_medicine011+$medicine011).$special_medicine011."</B></FONT><BR><FONT SIZE=2>".$medicine011."+(".$add_medicine011.")</FONT></td>\n";		
		echo "\t\t<td align=center valign=top><FONT SIZE=4  ".$color012."><B>".($add_medicine012+$medicine012).$special_medicine012."</B></FONT><BR><FONT SIZE=2>".$medicine012."+(".$add_medicine012.")</FONT></td>\n";	
		echo "\t\t<td align=center valign=top><FONT SIZE=4  ".$color013."><B>".($add_medicine013+$medicine013).$special_medicine013."</B></FONT><BR><FONT SIZE=2>".$medicine013."+(".$add_medicine013.")</FONT></td>\n";
		echo "\t\t<td align=center valign=top><FONT SIZE=4  ".$color014."><B>".($add_medicine014+$medicine014).$special_medicine014."</B></FONT><BR><FONT SIZE=2>".$medicine014."+(".$add_medicine014.")</FONT></td>\n";		
		echo "\t\t<td align=center valign=top><FONT SIZE=4  ".$color015."><B>".($add_medicine015+$medicine015).$special_medicine015."</B></FONT><BR><FONT SIZE=2>".$medicine015."+(".$add_medicine015.")</FONT></td>\n";		
		echo "\t\t<td align=center valign=top><FONT SIZE=4  ".$color016."><B>".($add_medicine016+$medicine016).$special_medicine016."</B></FONT><BR><FONT SIZE=2>".$medicine016."+(".$add_medicine016.")</FONT></td>\n";	
		echo "\t\t<td align=center valign=top><FONT SIZE=4  ".$color017."><B>".($add_medicine017+$medicine017).$special_medicine017."</B></FONT><BR><FONT SIZE=2>".$medicine017."+(".$add_medicine017.")</FONT></td>\n";			
		echo "\t\t<td align=center valign=top><FONT SIZE=4  ".$color018."><B>".($add_medicine018+$medicine018).$special_medicine018."</B></FONT><BR><FONT SIZE=2>".$medicine018."+(".$add_medicine018.")</FONT></td>\n";	
	
		echo "\t</tr>\n";
	}
	echo "</table>\n";
			
			
?>			
			</CENTER>
			</TD>
		</TR>
		<TR CLASS='fastq-green'>
			<TD  CLASS='fastq-green' COLSPAN=2  ALIGN=LEFT>
				
				<?php
				if ($flag_error == 'no'){
				?>
				<button type="submit" style='font-size:2em'>
					<img src="./img/green.jpg" alt="Confirm" width=30/>
					<FONT SIZE=5>ยืนยัน</FONT>
				</button>
				<?php
				}
				?>
			</TD>
</FORM>	
<FORM ACTION='medicine_transaction_management_volunteer.php'>			
			<TD  CLASS='fastq-green' COLSPAN=2  ALIGN=RIGHT>
				<button type=submit style='font-size:2em'>
					<img src="./img/red.jpg" alt="Cancel" width=30/>
					<FONT SIZE=5>ยกเลิก</FONT>
				</button>
			</TD>
		</TR>
	</TABLE>
</DIV>
</CENTER>
</FORM>	
<?php
} 
?>
	</BODY>
</HTML>