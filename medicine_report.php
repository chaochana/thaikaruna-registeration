﻿<?php
include('config.php');
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
<HTML xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="th">
	<HEAD>
		<TITLE>THAI KARUNA FOUNDATION</TITLE>
		<LINK rel="stylesheet" type="text/css" href="style.css">
	</HEAD>
	<BODY>
<?php

set_time_limit(0);

if ( $_GET[action] == 'medicineReport' )
{
	// Connecting, selecting database
	$link = mysql_connect($host, $uname, $passwd)
		or die('Could not connect: ' . mysql_error());
	mysql_select_db($database) or die('Could not select database');

	mysql_query("SET character_set_results=utf8");
	mysql_query("SET character_set_client=utf8");
	mysql_query("SET character_set_connection=utf8");

	// Performing SQL query
	if ( ! isset($_GET['Session']) ){ 
		$query = 'SELECT DISTINCT member.MemberID, Name, LastName, medicinetransaction.Transaction_Type, medicinetransaction.Queue, medicinetransaction.Remark FROM medicine, medicinetransaction, member, medicineorder WHERE member.memberID = medicineorder.memberID AND member.memberID=medicinetransaction.memberID AND medicine.medicineID=medicineorder.medicineID AND medicinetransaction.Date_IDX=\''.$_GET["date_IDX"].'\' ORDER by medicinetransaction.Queue LIMIT 0,1000';
	} else {
		$query = 'SELECT DISTINCT member.MemberID, Name, LastName, medicinetransaction.Transaction_Type, medicinetransaction.Queue, medicinetransaction.Remark FROM medicine, medicinetransaction, member, medicineorder WHERE member.memberID = medicineorder.memberID AND member.memberID=medicinetransaction.memberID AND medicine.medicineID=medicineorder.medicineID AND medicinetransaction.Date_IDX=\''.$_GET["date_IDX"].'\' ORDER by medicinetransaction.Queue AND medicineorder.Session=\''.$_GET["Session"].' LIMIT 0,1000';
	}

	echo $query;
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());

	// Printing results in HTML
	echo "ใบจัดยาประจำวันที่ ".$today;
	$page = 1;
	$seq = 1;
	$count = 1;
	while ($line = mysql_fetch_row($result))
	{
		if ( $count % 20 == 1 ){
			echo "PAGE: ".$page;
			echo "<TABLE CLASS='report' border=1 cellspacing=0 cellpadding=0 width=100%>\n";
			echo "\t<TR bgcolor=#666666>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>ลำดับ</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>รหัส</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>ชื่อ - นามสกุล</FONT></TD>";	
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>001<BR>เขียว</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>002<BR>ฟอก</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>003<BR>ปอด</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>004<BR>เบาหวาน</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>005<BR>มะกรูด</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>006<BR>น้ำผึ้ง</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>007<BR>มะนาว</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>008<BR>กระดูกหมู</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>009<BR>มะพร้าว</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>010<BR>ตำลึง</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>011<BR>หยอดตา</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>012<BR>ตัด</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>013<BR>ดูดหนอง</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>014<BR>ไพลประคบ</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>015<BR>ไพลน้ำ</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>016<BR>ทาผิว</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>017<BR>ระบาย</FONT></TD>";			
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>018<BR>รมตา</FONT></TD>";		
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>019<BR>พริกไทย</FONT></TD>";			
			echo "\t</TR>";
		}

		// Performing SQL query
		$query2 = 'SELECT medicineorder.medicineID, medicineName, Amount, Add_Amount FROM medicine, medicineorder, member WHERE member.memberID=medicineorder.memberID AND medicine.medicineID=medicineorder.medicineID AND medicineorder.OrderDate=\''.$_GET["date"].'\' AND member.MemberID=\''.$line[0].'\' ORDER by medicineorder.RecIDX LIMIT 0 , 30';
		$result2 = mysql_query($query2) or die('Query failed: ' . mysql_error());

		// Printing results in HTML
		echo "\t<TR>\n";
		
		echo "\t\t<td>".$seq."</td>\n";		
		echo "\t\t<td>".$line[0]."</td>\n";
		echo "\t\t<td>".$line[1]." ".$line[2]."</td>\n";

		$medicine001 = "&nbsp;";
		$medicine002 = "&nbsp;";
		$medicine003 = "&nbsp;";
		$medicine004 = "&nbsp;";
		$medicine005 = "&nbsp;";
		$medicine006 = "&nbsp;";
		$medicine007 = "&nbsp;";
		$medicine008 = "&nbsp;";
		$medicine009 = "&nbsp;";
		$medicine010 = "&nbsp;";
		$medicine011 = "&nbsp;";
		$medicine012 = "&nbsp;";
		$medicine013 = "&nbsp;";
		$medicine014 = "&nbsp;";
		$medicine015 = "&nbsp;";
		$medicine016 = "&nbsp;";
		$medicine017 = "&nbsp;";
		$medicine018 = "&nbsp;";
		$medicine019 = "&nbsp;";
		
		while ($row = mysql_fetch_object($result2)) {
			$total = $row->Amount + $row->Add_Amount;

			switch ( $row->medicineID )
			{
				case '001': $medicine001 = $total; break;
				case '002': $medicine002 = $total; break;
				case '003': $medicine003 = $total; break;
				case '004': $medicine004 = $total; break;
				case '005': $medicine005 = $total; break;
				case '006': $medicine006 = $total; break;
				case '007': $medicine007 = $total; break;
				case '008': $medicine008 = $total; break;
				case '009': $medicine009 = $total; break;
				case '010': $medicine010 = $total; break;
				case '011': $medicine011 = $total; break;
				case '012': $medicine012 = $total; break;
				case '013': $medicine013 = $total; break;
				case '014': $medicine014 = $total; break;
				case '015': $medicine015 = $total; break;
				case '016': $medicine016 = $total; break;
				case '017': $medicine017 = $total; break;
				case '018': $medicine018 = $total; break;
				case '019':	$medicine019 = $total; break;				
			}
		}

		echo "\t\t<td align=center>".$medicine001."</td>\n";
		echo "\t\t<td align=center>".$medicine002."</td>\n";
		echo "\t\t<td align=center>".$medicine003."</td>\n";
		echo "\t\t<td align=center bgcolor=#666666>".$medicine004."</td>\n";
		echo "\t\t<td align=center>".$medicine005."</td>\n";
		echo "\t\t<td align=center>".$medicine006."</td>\n";
		echo "\t\t<td align=center>".$medicine007."</td>\n";
		echo "\t\t<td align=center bgcolor=#666666>".$medicine008."</td>\n";
		echo "\t\t<td align=center>".$medicine009."</td>\n";
		echo "\t\t<td align=center>".$medicine010."</td>\n";
		echo "\t\t<td align=center>".$medicine011."</td>\n";
		echo "\t\t<td align=center>".$medicine012."</td>\n";
		echo "\t\t<td align=center bgcolor=#666666>".$medicine013."</td>\n";
		echo "\t\t<td align=center>".$medicine014."</td>\n";
		echo "\t\t<td align=center>".$medicine015."</td>\n";
		echo "\t\t<td align=center>".$medicine016."</td>\n";
		echo "\t\t<td align=center>".$medicine017."</td>\n";	
		echo "\t\t<td align=center>".$medicine018."</td>\n";		
		echo "\t\t<td align=center>".$medicine019."</td>\n";
		echo "\t</TR>\n";

		if ( $count % 20 == 0 )
		{
			echo "</TABLE>\n";
			$page++;
			$count = 1;
		} else 
		{
			$count++;	
		}
	
		$seq++;
	}

	// Free resultset
	mysql_free_result($result);
	mysql_free_result($result2);

	// Closing connection
	mysql_close($link);
} else if ( $_GET[action] == 'medicineReportByQueue' )
{	
	// Connecting, selecting database
	$link = mysql_connect($host, $uname, $passwd)
		or die('Could not connect: ' . mysql_error());
	mysql_select_db($database) or die('Could not select database');

	mysql_query("SET character_set_results=utf8");
	mysql_query("SET character_set_client=utf8");
	mysql_query("SET character_set_connection=utf8");

	if($_GET['Queue_Session']=='Volunteer1' && $_GET['Unit']!='all'){
		$query = 'SELECT member.MemberID, Title, Name, LastName, medicinetransaction.Transaction_Type, medicinetransaction.Queue, medicinetransaction.Remark, medicinetransaction.Unit FROM medicine, medicinetransaction, member WHERE member.memberID=medicinetransaction.memberID AND medicinetransaction.Date_IDX=\''.$_GET["date_IDX"].'\' AND medicinetransaction.Queue BETWEEN \''.$_GET['StartQueue'].'\' AND \''.$_GET['EndQueue'].'\' AND medicinetransaction.Queue_Session = \''.$_GET['Queue_Session'].'\' AND medicinetransaction.Unit = \''.$_GET['Unit'].'\' GROUP BY member.MemberID ORDER by medicinetransaction.Queue LIMIT 0 , 480 ';	
		} else {
		$query = 'SELECT member.MemberID, Title, Name, LastName, medicinetransaction.Transaction_Type, medicinetransaction.Queue, medicinetransaction.Remark, medicinetransaction.Unit FROM member, medicinetransaction WHERE member.memberID=medicinetransaction.memberID AND medicinetransaction.Date_IDX=\''.$_GET["date_IDX"].'\' AND medicinetransaction.Queue BETWEEN \''.$_GET['StartQueue'].'\' AND \''.$_GET['EndQueue'].'\' AND medicinetransaction.Queue_Session = \''.$_GET['Queue_Session'].'\' GROUP BY member.MemberID ORDER by medicinetransaction.Queue LIMIT 0 , 480;';

	}

	$result = mysql_query($query) or die('Query failed: ' . mysql_error());

	// Printing results in HTML
	echo "ใบจัดยาประจำวันที่ ".$today;
	$page = 1;
	$seq = 1;
	$count = 1;
	while ( $line = mysql_fetch_row($result) )
	{
		if ( $count % 20 == 1 )
		{
			echo "PAGE: ".$page;
			echo "<TABLE CLASS='report' border=1 cellspacing=0 cellpadding=0 width=100%>\n";
			echo "\t<TR bgcolor=#666666>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>ลำดับ</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>รหัส</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>ชื่อ - นามสกุล</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>การขอรับสมุนไพร</FONT></TD>";
			if($_GET['Queue_Session']=='Volunteer1')
			{
				echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>แผนก</FONT></TD>";
			}
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>001<BR>เขียว</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>002<BR>ฟอก</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>003<BR>ปอด</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>004<BR>เบาหวาน</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>005<BR>มะกรูด</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>006<BR>น้ำผึ้ง</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>007<BR>มะนาว</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>008<BR>กระดูกหมู</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>009<BR>มะพร้าว</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>010<BR>ตำลึง</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>011<BR>หยอดตา</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>012<BR>ตัด</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>013<BR>ดูดหนอง</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>014<BR>ไพลประคบ</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>015<BR>ไพลน้ำ</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>016<BR>ทาผิว</FONT></TD>";
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>017<BR>ระบาย</FONT></TD>";	
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>018<BR>รมตา</FONT></TD>";				
			echo "\t\t<TD align=center><FONT SIZE='' COLOR='#FFFFFF'>019<BR>พริกไทย</FONT></TD>";
			echo "\t</TR>";
		}

		// Performing SQL query
		$query2 = 'SELECT medicineorder.medicineID, medicineName, Amount, Add_Amount, special FROM medicine, medicineorder, member WHERE member.memberID=medicineorder.memberID AND medicine.medicineID=medicineorder.medicineID AND medicineorder.OrderDate=\''.$_GET["date"].'\' AND member.MemberID=\''.$line[0].'\' ORDER by medicineorder.RecIDX LIMIT 0 , 30';

		//echo $query2;
		$result2 = mysql_query($query2) or die('Query failed: ' . mysql_error());

		// Printing results in HTML

	$queue = $line[5];
	echo "\t<TR>\n";
	
	echo "\t\t<td>".$queue."</td>\n";		
	echo "\t\t<td>".$line[0]."</td>\n";
	echo "\t\t<td>".$line[1]." ".$line[2]." ".$line[3]."</td>\n";
	if (strtolower($line[4]) == 'twoweeks'){
		echo "\t\t<td>สองสัปดาห์</td>\n";		
	} else if (strtolower($line[4]) == 'represent') {
		echo "\t\t<td>มีผู้รับแทน</td>\n";
	} else if (strtolower($line[4]) == 'represent_twoweeks') {
		echo "\t\t<td>มีผู้รับแทน และ สองสัปดาห์</td>\n";
	} else if (strtolower($line[4]) == 'double') {
		echo "\t\t<td>สองชุดต่อหนึ่งสัปดาห์</td>\n";
	} else {
		echo "\t\t<td>&nbsp;</td>\n";
	}
	
	if($_GET['Queue_Session']=='Volunteer1'){
		switch ($line[7])
		{
			case "coconut":			echo "\t\t<td>1.มะพร้าว</td>\n"; break;
			case "drinkingwater":	echo "\t\t<td>2.โรงน้ำดื่มไทยกรุณา</td>\n"; break;
			case "stock":			echo "\t\t<td>3.stock</td>\n"; break;	
			case "generalpack":		echo "\t\t<td>4.จัดสมุนไพรทั่วไป</td>\n"; break;	
			case "garlic":			echo "\t\t<td>5.กระเทียม</td>\n"; break;	
			case "rawherb":			echo "\t\t<td>6.เด็ดใบยา</td>\n"; break;		
			case "liquidherb":		echo "\t\t<td>7.บรรจุยาน้ำ</td>\n"; break;		
			case "pasteherb":		echo "\t\t<td>8.ยาลูกกลอน</td>\n"; break;			
			case "env":				echo "\t\t<td>9.สิ่งแวดล้อม</td>\n"; break;	
			case "bottle":			echo "\t\t<td>10.ล้างขวด</td>\n"; break;	
			case "kaffirlime":		echo "\t\t<td>11.มะกรูด</td>\n"; break;		
			case "bio":				echo "\t\t<td>12.ชีวภาพ</td>\n"; break;		
			case "iravadee":		echo "\t\t<td>13.อิรวดี</td>\n"; break;		
			case "uncle":			echo "\t\t<td>14.น้าหงำ</td>\n"; break;	
			case "steam":			echo "\t\t<td>15.ห้องอบ-ห้องน้ำ</td>\n"; break;		
			case "construction":	echo "\t\t<td>16.โยธา</td>\n"; break;		
			case "traffic":			echo "\t\t<td>17.จราจร</td>\n"; break;				
			case "greenherb":		echo "\t\t<td>18.ยาเขียว</td>\n"; break;
			case "registra":		echo "\t\t<td>19.ทะเบียน</td>\n"; break;		
			case "general":			echo "\t\t<td>20.ทั่วไป</td>\n"; break;					
			default: 				echo "\t\t<td><ไม่ได้ระบุ></td>\n"; break;			
		}	
	}
	
	$medicine001 = "&nbsp;";
	$medicine002 = "&nbsp;";
	$medicine003 = "&nbsp;";
	$medicine004 = "&nbsp;";
	$medicine005 = "&nbsp;";
	$medicine006 = "&nbsp;";
	$medicine007 = "&nbsp;";
	$medicine008 = "&nbsp;";
	$medicine009 = "&nbsp;";
	$medicine010 = "&nbsp;";
	$medicine011 = "&nbsp;";
	$medicine012 = "&nbsp;";
	$medicine013 = "&nbsp;";
	$medicine014 = "&nbsp;";
	$medicine015 = "&nbsp;";
	$medicine016 = "&nbsp;";
	$medicine017 = "&nbsp;";
	$medicine018 = "&nbsp;";
	$medicine019 = "&nbsp;";
	
	while ($row = mysql_fetch_object($result2)) {
		$total = $row->Amount + $row->Add_Amount;

		switch ( $row->medicineID )
		{
			case '001':	$medicine001 = $total; break;
			case '002': $medicine002 = $total; break;
			case '003': $medicine003 = $total; break;
			case '004': $medicine004 = $total; break;
			case '005': $medicine005 = $total; break;
			case '006': $medicine006 = $total; break;
			case '007': $medicine007 = $total; break;
			case '008': $medicine008 = $total; break;
			case '009': $medicine009 = $total; break;
			case '010': $medicine010 = $total; break;
			case '011': $medicine011 = $total; break;
			case '012':	$medicine012 = $total; break;
			case '013': $medicine013 = $total; break;
			case '014': $medicine014 = $total; break;
			case '015': $medicine015 = $total; break;
			case '016': $medicine016 = $total; break;
			case '017': $medicine017 = $total; break;
			case '018': $medicine018 = $total; break;
			case '019': $medicine019 = $total; break;				
		}
			
		switch ( $row->special )
		{
			case 'เขียวเต็ม': 	$medicine001 = $medicine001." เต็ม"; break;
			case 'ปอดต้มเอง':	$medicine003 = $medicine003." ต้มเอง"; break;
			case 'เบาหวานต้มเอง': $medicine004 = $medicine004." ต้มเอง"; break;
			case 'ดำพิเศษ':		$medicine006 = $medicine006." พิ"; break;
			case 'เข้มข้น':		$medicine008 = $medicine008." ข้น"; break;
			case 'มะพร้าวต้มเอง':	$medicine009 = $medicine009." ต้มเอง"; break;
			case 'ตำลึงเต็ม':	$medicine010 = $medicine010." เต็ม"; break;
		}		
	}
	echo "\t\t<td align=center>".$medicine001."</td>\n";
	echo "\t\t<td align=center>".$medicine002."</td>\n";
	echo "\t\t<td align=center>".$medicine003."</td>\n";
	echo "\t\t<td align=center>".$medicine004."</td>\n";
	echo "\t\t<td align=center>".$medicine005."</td>\n";
	echo "\t\t<td align=center>".$medicine006."</td>\n";
	echo "\t\t<td align=center>".$medicine007."</td>\n";
	echo "\t\t<td align=center>".$medicine008."</td>\n";
	echo "\t\t<td align=center>".$medicine009."</td>\n";
	echo "\t\t<td align=center>".$medicine010."</td>\n";
	echo "\t\t<td align=center>".$medicine011."</td>\n";
	echo "\t\t<td align=center>".$medicine012."</td>\n";
	echo "\t\t<td align=center>".$medicine013."</td>\n";
	echo "\t\t<td align=center>".$medicine014."</td>\n";
	echo "\t\t<td align=center>".$medicine015."</td>\n";
	echo "\t\t<td align=center>".$medicine016."</td>\n";
	echo "\t\t<td align=center>".$medicine017."</td>\n";		
	echo "\t\t<td align=center>".$medicine018."</td>\n";		
	echo "\t\t<td align=center>".$medicine019."</td>\n";		
	echo "\t</TR>\n";

	if ( $count % 20 == 0 ) {
		echo "</TABLE>\n";
		$page++;
		$count = 1;
	} else {
		$count++;	
	}
	
	$seq++;
	mysql_free_result($result2);
		}

	

	//=======================================================



	// Free resultset
	mysql_free_result($result);

	// Closing connection
	mysql_close($link);	

} 

flush();
?>
</BODY>
</HTML>