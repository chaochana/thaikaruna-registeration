<?php
include('config.php');
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
<HTML xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="th">
	<HEAD>
		<TITLE>THAI KARUNA FOUNDATION</TITLE>
		<LINK rel="stylesheet" type="text/css" href="style.css">
	</HEAD>
	<BODY>
<?php
include("header.php");
?>
	<HR>
	<BR>
	
<?php
	$JAN = "<OPTION VALUE='01'>January";
	$FEB = "<OPTION VALUE='02'>Febuary";
	$MAR = "<OPTION VALUE='03'>March";
	$APR = "<OPTION VALUE='04'>April";
	$MAY = "<OPTION VALUE='05'>May";
	$JUN = "<OPTION VALUE='06'>June";
	$JUL = "<OPTION VALUE='07'>July";
	$AUG = "<OPTION VALUE='08'>August";
	$SEP = "<OPTION VALUE='09'>September";
	$OCT = "<OPTION VALUE='10'>October";
	$NOV = "<OPTION VALUE='11'>November";
	$DEC = "<OPTION VALUE='12'>December";
	
	if(isset($_GET['MONTH'])) {
		$month = $_GET['MONTH'];
	} else {
		$month = date(m);
	}
	
	switch ($month) {
		case '01': $JAN = "<OPTION VALUE='01' SELECTED >January"; break;
		case '02': $FEB = "<OPTION VALUE='02' SELECTED >Febuary"; break;
		case '03': $MAR = "<OPTION VALUE='03' SELECTED >March"; break;
		case '04': $APR = "<OPTION VALUE='04' SELECTED >April"; break;
		case '05': $MAY = "<OPTION VALUE='05' SELECTED >May"; break;
		case '06': $JUN = "<OPTION VALUE='06' SELECTED >June"; break;
		case '07': $JUL = "<OPTION VALUE='07' SELECTED >July"; break;
		case '08': $AUG = "<OPTION VALUE='08' SELECTED >August"; break;
		case '09': $SEP = "<OPTION VALUE='09' SELECTED >September"; break;
		case '10': $OCT = "<OPTION VALUE='10' SELECTED >October"; break;
		case '11': $NOV = "<OPTION VALUE='11' SELECTED >November"; break;
		case '12': $DEC = "<OPTION VALUE='12' SELECTED >December"; break;
	}
	echo "<FORM NAME='DATE' METHOD='GET'>";
	echo "<SELECT NAME='MONTH'>\n";
	echo $JAN."\n";
	echo $FEB."\n";
	echo $MAR."\n";
	echo $APR."\n";
	echo $MAY."\n";
	echo $JUN."\n";
	echo $JUL."\n";
	echo $AUG."\n";
	echo $SEP."\n";
	echo $OCT."\n";
	echo $NOV."\n";
	echo $DEC."\n";
	echo "</SELECT>\n";
?>
	
	<SELECT NAME='YEAR'>
		<OPTION VALUE='2009'>2009</OPTION>
		<OPTION VALUE='2010'>2010</OPTION>
		<OPTION VALUE='2011'>2011</OPTION>
		<OPTION VALUE='2012'>2012</OPTION>
		<OPTION VALUE='2013'>2013</OPTION>
		<OPTION VALUE='2014' SELECTED>2014</OPTION>
		<OPTION VALUE='2015'>2015</OPTION>
		<OPTION VALUE='2016'>2016</OPTION>		
	</SELECT>
	<INPUT TYPE='SUBMIT'>
	<BR>
	<TABLE width='400' border=1>
	<?php 
	if(isset($_GET['MONTH'])&&isset($_GET['YEAR'])){
	$start_month = $_GET['MONTH'];
	$start_year = $_GET['YEAR'];
	
	$start_date = mktime(0, 0, 0,$start_month, 1, $start_year);

	$days_in_month = date('t', $start_date);
	$month_first_day = date('w', $start_date);
	$start_month = date('M', $start_date);
	$start_year = $_GET['YEAR'];
	?>
	<TR><TH colspan=7><?php echo "<A href=report.php?date=".$start_year.$_GET['MONTH']."% TARGET=_BLANK>".$start_month."</A> | <A href=report.php?date=".$start_year."% TARGET=_BLANK>".$start_year ?></TH></TR>
	<TR><TH bgcolor=yellow>Sun</TH><TH>Mon</TH><TH>Tue</TH><TH>Wed</TH><TH bgcolor=yellow>Thu</TH><TH>Fri</TH><TH>Sat</TH></TR>

<?php
	#$start_month = 12;
	#$start_year = 2009;
	

	
	#echo $days_in_month;
	#echo $month_first_day;
	
	$j = $month_first_day;
	$num_weeks = 1;
	
	$l = 1;
	
	echo "<TR>";
	$i = 1;
	while($i <= ($days_in_month + $month_first_day)) {
		if($i >= ($month_first_day + 1)){
			if ($l < 10){
				$l = "0".$l;
			}
			if ($j == 0 || $j == 4){
    				echo "<TD bgcolor=yellow><A HREF=report.php?date=".$start_year.$_GET['MONTH'].$l." TARGET=_BLANK>".$l."</A></TD>";
    			} else {
    				echo "<TD>".$l."</TD>";
    			}
    			$l++;
    			$i++;
    			$j++;
    			if($j == 7) {
        			$j = 0;
        			$num_weeks++;
        			echo "</TR>\n<TR>";
    			}
    		} else {
    			echo "<TD></TD>";
    			$i++;
    		}
    		
	}

// if the last day of the month happens to be a Saturday,
// take one off the number of weeks
// because it was being added inside the for loop.
	if ($j == 0) {
    		$num_weeks--;
	} 
	}
?>
	</TABLE>
</BODY>
</HTML>