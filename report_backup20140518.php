﻿<?php

include('config.php');
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
<HTML xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="th">
	<HEAD>
		<TITLE>THAI KARUNA FOUNDATION</TITLE>
		<LINK rel="stylesheet" type="text/css" href="style.css">
	</HEAD>
	<BODY>
<?php


$date_show=$date_apply=substr($_GET['date'],0,4)."-".substr($_GET['date'],4,2)."-".substr($_GET['date'],6,2);
?>
	<H1>รายงานประจำวันที่ (ปี-เดือน-วัน) <?php echo $date_show ?> เวลา <?php echo date('H:i A'); ?> </H1>
<?php

	// Connecting, selecting database
	$link = mysql_connect($host, $uname, $passwd)
		or die('Could not connect: ' . mysql_error());
	mysql_select_db($database) or die('Could not select database');

	mysql_query("SET character_set_results=utf8");
	mysql_query("SET character_set_client=utf8");
	mysql_query("SET character_set_connection=utf8");	

	if(isset($_GET['date'])){
		$date=$_GET['date'];
	} else {
		$date=$today_IDX;
	}

	//===== Get New Member ==================================================================

	$query_all_patient = 'SELECT MemberID FROM medicinetransaction WHERE medicinetransaction.DATE_IDX=\''.$date.'\'';

	$all_patient_result = mysql_query($query_all_patient) or die('Query failed: ' . mysql_error());

	$all_patient = mysql_num_rows($all_patient_result);
	
	mysql_free_result($all_patient_result);

	//===== Get Normal Number ==================================================================

	$query_normal_patient = 'SELECT MemberID FROM medicinetransaction WHERE medicinetransaction.DATE_IDX=\''.$date.'\' AND Transaction_Type LIKE \'Normal\'';

	$normal_patient_result = mysql_query($query_normal_patient) or die('Query failed: ' . mysql_error());

	$normal_patient = mysql_num_rows($normal_patient_result);

	mysql_free_result($normal_patient_result);	
	
	//===== Get TWO WEEKS Number ==================================================================

	$query_twoweeks_patient = 'SELECT MemberID FROM medicinetransaction WHERE medicinetransaction.DATE_IDX=\''.$date.'\' AND Transaction_Type LIKE \'TwoWeeks\'';

	$twoweeks_patient_result = mysql_query($query_twoweeks_patient) or die('Query failed: ' . mysql_error());

	$twoweeks_patient = mysql_num_rows($twoweeks_patient_result);

	//===== Get DOUBLE Number ==================================================================

	$query_double_patient = 'SELECT MemberID FROM medicinetransaction WHERE medicinetransaction.DATE_IDX=\''.$date.'\' AND Transaction_Type LIKE \'Double\'';

	$double_patient_result = mysql_query($query_double_patient) or die('Query failed: ' . mysql_error());

	$double_patient = mysql_num_rows($double_patient_result);	
	
	mysql_free_result($double_patient_result);	
	
	//===== Get Represent Number ==================================================================

	$query_represent_patient = 'SELECT MemberID FROM medicinetransaction WHERE medicinetransaction.DATE_IDX=\''.$date.'\' AND Transaction_Type LIKE \'Represent\'';

	$represent_patient_result = mysql_query($query_represent_patient) or die('Query failed: ' . mysql_error());

	$represent_patient = mysql_num_rows($represent_patient_result);

	mysql_free_result($represent_patient_result);
	
	//===== Get Represent and TWO WEEKS Number ==================================================================

	$query_represent_twoweeks_patient = 'SELECT MemberID FROM medicinetransaction WHERE medicinetransaction.DATE_IDX=\''.$date.'\' AND Transaction_Type LIKE \'Represent_TwoWeeks\'';

	$represent_twoweeks_patient_result = mysql_query($query_represent_twoweeks_patient) or die('Query failed: ' . mysql_error());

	$represent_twoweeks_patient = mysql_num_rows($represent_twoweeks_patient_result);

	mysql_free_result($represent_twoweeks_patient_result);
	
	//===== Get Morning Session Number ==================================================================

	$query_morning_patient = 'SELECT MemberID FROM medicinetransaction WHERE medicinetransaction.DATE_IDX=\''.$date.'\' AND Queue_Session LIKE \'Morning\'';

	$morning_patient_result = mysql_query($query_morning_patient) or die('Query failed: ' . mysql_error());

	$morning_patient = mysql_num_rows($morning_patient_result);

	mysql_free_result($morning_patient_result);
	
	//===== Get Afternoon Session Number ==================================================================

	$query_afternoon_patient = 'SELECT MemberID FROM medicinetransaction WHERE medicinetransaction.DATE_IDX=\''.$date.'\' AND Queue_Session LIKE \'Afternoon\'';

	$afternoon_patient_result = mysql_query($query_afternoon_patient) or die('Query failed: ' . mysql_error());

	$afternoon_patient = mysql_num_rows($afternoon_patient_result);
	
	mysql_free_result($afternoon_patient_result);
	
	//===== Get Volunteer Session Number ==================================================================

	$query_volunteer_patient = 'SELECT MemberID FROM medicinetransaction WHERE medicinetransaction.DATE_IDX=\''.$date.'\' AND Queue_Session LIKE \'Volunteer%\'';

	$volunteer_patient_result = mysql_query($query_volunteer_patient) or die('Query failed: ' . mysql_error());

	$volunteer_patient = mysql_num_rows($volunteer_patient_result);
	
	mysql_free_result($volunteer_patient_result);
	
	//===== Get Vip Session Number ==================================================================

	$query_vip_patient = 'SELECT MemberID FROM medicinetransaction WHERE medicinetransaction.DATE_IDX=\''.$date.'\' AND Queue_Session LIKE \'VIP%\'';

	$volunteer_vip_result = mysql_query($query_vip_patient) or die('Query failed: ' . mysql_error());

	$vip_patient = mysql_num_rows($volunteer_vip_result);	
	
	mysql_free_result($volunteer_vip_result);	
	
	//===== Get Cancer Session Number ==================================================================

	$query_cancer_patient = 'SELECT MemberID FROM medicinetransaction WHERE medicinetransaction.DATE_IDX=\''.$date.'\' AND Queue_Session LIKE \'Cancer%\'';

	$volunteer_cancer_result = mysql_query($query_cancer_patient) or die('Query failed: ' . mysql_error());

	$cancer_patient = mysql_num_rows($volunteer_cancer_result);		

	mysql_free_result($volunteer_cancer_result);	
	
	//===== Get Chumporn Session Number ==================================================================

	$query_chumporn_patient = 'SELECT MemberID FROM medicinetransaction WHERE medicinetransaction.DATE_IDX=\''.$date.'\' AND Queue_Session LIKE \'Chumporn%\'';

	$chumporn_result = mysql_query($query_chumporn_patient) or die('Query failed: ' . mysql_error());

	$chumporn_patient = mysql_num_rows($chumporn_result);	

	mysql_free_result($chumporn_result);	
	
	//===== Get Military Session Number ==================================================================

	$query_military_patient = 'SELECT MemberID FROM medicinetransaction WHERE medicinetransaction.DATE_IDX=\''.$date.'\' AND Queue_Session LIKE \'Military%\'';

	$military_result = mysql_query($query_military_patient) or die('Query failed: ' . mysql_error());

	$military_patient = mysql_num_rows($military_result);		
	
	mysql_free_result($military_result);	
	
	//===== Get Volunteer1 Session Number ==================================================================

	$query_volunteer1_patient = 'SELECT MemberID FROM medicinetransaction WHERE medicinetransaction.DATE_IDX=\''.$date.'\' AND Queue_Session LIKE \'Volunteer1%\'';

	$volunteer1_result = mysql_query($query_volunteer1_patient) or die('Query failed: ' . mysql_error());

	$volunteer1_patient = mysql_num_rows($volunteer1_result);			

	mysql_free_result($volunteer1_result);
	
	//===== Get Volunteer2 Session Number ==================================================================

	$query_volunteer2_patient = 'SELECT MemberID FROM medicinetransaction WHERE medicinetransaction.DATE_IDX=\''.$date.'\' AND Queue_Session LIKE \'Volunteer2%\'';

	$volunteer2_result = mysql_query($query_volunteer2_patient) or die('Query failed: ' . mysql_error());

	$volunteer2_patient = mysql_num_rows($volunteer2_result);		

	mysql_free_result($volunteer2_result);
	
	//===== Get New Member ==================================================================

	if(isset($_GET['date'])){
		$date_apply=substr($_GET['date'],0,4)."-".substr($_GET['date'],4,2)."-".substr($_GET['date'],6,2);
		//echo $date_apply;
	} else {
		$date_apply=$today_apply;
	}
	
	$query_new_patient = 'SELECT MemberID FROM member WHERE DateApply=\''.$date_apply.'\'';

	$new_patient_result = mysql_query($query_new_patient) or die('Query failed: ' . mysql_error());

	$new_patient = mysql_num_rows($new_patient_result);

	echo "<pre>";
	echo "คนไข้ทั้งหมดของวันนี้: ".$all_patient;
	echo " ในจำนวนนี้เป็นคนไข้ใหม่(ครบห้าครั้ง เปลี่ยนเป็นครั้งที่หก): ".$new_patient."<BR />";

	echo "ในจำนวนข้างต้นมีผู้ขอสมุนไพร:<BR>";
	echo " - แบ่งตามคิว";
	echo "\t\tคิวเช้า: ".$morning_patient;
	echo "\tคิวบ่าย: ".$afternoon_patient;
	echo "\tคิวอาสาสมัคร: ".$volunteer_patient;
	echo "\tคิวVIP: ".$vip_patient;
	echo "\tคิวมะเร็ง: ".$cancer_patient;	
	echo "\tคิวชุมพร: ".$chumporn_patient;
	echo "\tคิวทหาร: ".$military_patient;
	echo "<HR>";
	echo " - แบ่งตามประเภทการขอรับสมุนไพร";
	echo "\tปกติ: ".$normal_patient;
	echo "\tสองสัปดาห์: ".$twoweeks_patient;
	echo "\tรับยาแทน: ".$represent_patient;
	echo "\tรับยาแทน และ สองสัปดาห์: ".$represent_twoweeks_patient;
	echo "\tรับยาสองชุดต่อหนึ่งสัปดาห์: ".$double_patient;	
	echo "<HR>";
	echo "</pre>";


	// Performing SQL query

//	$query = 'SELECT Amount, Add_Amount FROM medicineorder WHERE medicineorder.OrderDate=\''.$_GET["date"].'\' AND medicineID=\'001\'';
	
	//echo $_GET['date'];
	
	echo "<H2>คนไข้ทั้งหมดของวันนี้: ".$all_patient." คน";
	
	echo "<table border=1>\n";
	echo "\t<TR>\n";
	echo "\t\t<td>ชื่อ</td>\n";
	echo "\t\t<td>จำนวนที่ขอประจำ</td>\n";
	echo "\t\t<td>จำนวนที่ขอเพิ่ม</td>\n";
	echo "\t\t<td>รวม</td>\n";	
	echo "\t</TR>\n";
	
	$query = 'SELECT medicine.medicineId FROM medicine';
	
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());

	while ($line = mysql_fetch_row($result)) {
		$query2 = 'SELECT medicine.medicineName, SUM(Amount), SUM(Add_Amount) FROM medicineorder, medicine WHERE Date_IDX LIKE \''.$_GET["date"].'\' AND medicine.medicineId = medicineorder.medicineId AND medicineorder.medicineID = \''.$line[0].'\' GROUP BY medicine.medicineName LIMIT 0,10';
		
		//echo $query2."<BR>";

		$result2 = mysql_query($query2) or die('X Query failed: ' . mysql_error());

		while ($line = mysql_fetch_array($result2, MYSQL_ASSOC)) {
			echo "\t<tr>\n";
			$total = 0;
			foreach ($line as $col_value) {
				echo "\t\t<td>$col_value</td>\n";
				
				if ( is_numeric($col_value) ){
					$total = $total + $col_value;
				}
				
			}
			echo "\t\t<td>".$total."</td>\n";
			echo "\t</tr>\n";
		}

		mysql_free_result($result2);
	}


	// Free resultset
	mysql_free_result($result);

	echo "</table>\n";

	/*---------------------------- End all medicine -------------------------------*/
	echo "<DIV STYLE='page-break-after: always;'></DIV>";

	echo "<H2>คนไข้ VIP: ".$vip_patient." คน";
	
	echo "<table border=1>\n";
	echo "\t<TR>\n";
	echo "\t\t<td>ชื่อ</td>\n";
	echo "\t\t<td>จำนวนที่ขอประจำ</td>\n";
	echo "\t\t<td>จำนวนที่ขอเพิ่ม</td>\n";
	echo "\t\t<td>รวม</td>\n";	
	echo "\t</TR>\n";
	
	$query = 'SELECT medicine.medicineId FROM medicine';
	
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());

	while ($line = mysql_fetch_row($result)) {
		$query2 = 'SELECT medicine.medicineName, SUM(Amount), SUM(Add_Amount) FROM medicineorder, medicine, medicinetransaction';
		$query2 = $query2.' WHERE medicineorder.Date_IDX LIKE \''.$_GET["date"].'\'';
		$query2 = $query2.' AND medicine.medicineId = medicineorder.medicineId';
		$query2 = $query2.' AND medicinetransaction.Transaction_ID = medicineorder.Transaction_ID';
		$query2 = $query2.' AND medicineorder.medicineID = \''.$line[0].'\'';
		$query2 = $query2.' AND medicinetransaction.Queue_Session = \'VIP\'';
		$query2 = $query2.' GROUP BY medicine.medicineName';
		
		//echo $query2."<BR>";
		
		$result2 = mysql_query($query2) or die('X Query failed: ' . mysql_error());

		while ($line = mysql_fetch_array($result2, MYSQL_ASSOC)) {
			echo "\t<tr>\n";
			$total = 0;
			foreach ($line as $col_value) {
				echo "\t\t<td>$col_value</td>\n";
				
				if ( is_numeric($col_value) ){
					$total = $total + $col_value;
				}
				
			}
			echo "\t\t<td>".$total."</td>\n";
			echo "\t</tr>\n";
		}

		mysql_free_result($result2);
	}

	// Free resultset
	mysql_free_result($result);

	echo "</table>\n";

	/*---------------------------- End VIP -------------------------------*/

	echo "<DIV STYLE='page-break-after: always;'></DIV>";

	echo "<H2>คนไข้มะเร็ง: ".$cancer_patient." คน";
	
	echo "<table border=1>\n";
	echo "\t<TR>\n";
	echo "\t\t<td>ชื่อ</td>\n";
	echo "\t\t<td>จำนวนที่ขอประจำ</td>\n";
	echo "\t\t<td>จำนวนที่ขอเพิ่ม</td>\n";
	echo "\t\t<td>รวม</td>\n";	
	echo "\t</TR>\n";
	
	$query = 'SELECT medicine.medicineId FROM medicine';
	
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());

	while ($line = mysql_fetch_row($result)) {
		$query2 = 'SELECT medicine.medicineName, SUM(Amount), SUM(Add_Amount) FROM medicineorder, medicine, medicinetransaction';
		$query2 = $query2.' WHERE medicineorder.Date_IDX LIKE \''.$_GET["date"].'\'';
		$query2 = $query2.' AND medicine.medicineId = medicineorder.medicineId';
		$query2 = $query2.' AND medicinetransaction.Transaction_ID = medicineorder.Transaction_ID';
		$query2 = $query2.' AND medicineorder.medicineID = \''.$line[0].'\'';
		$query2 = $query2.' AND medicinetransaction.Queue_Session = \'Cancer\'';
		$query2 = $query2.' GROUP BY medicine.medicineName';
		
		//echo $query2."<BR>";

		$result2 = mysql_query($query2) or die('X Query failed: ' . mysql_error());

		while ($line = mysql_fetch_array($result2, MYSQL_ASSOC)) {
			echo "\t<tr>\n";
			$total = 0;
			foreach ($line as $col_value) {
				echo "\t\t<td>$col_value</td>\n";
				
				if ( is_numeric($col_value) ){
					$total = $total + $col_value;
				}
				
			}
			echo "\t\t<td>".$total."</td>\n";
			echo "\t</tr>\n";
		}

		mysql_free_result($result2);
	}

	// Free resultset
	mysql_free_result($result);

	echo "</table>\n";

	/*---------------------------- End Cancer -------------------------------*/

	echo "<DIV STYLE='page-break-after: always;'></DIV>";

	echo "<H2>คนไข้ชุมพร: ".$chumporn_patient." คน";
	
	echo "<table border=1>\n";
	echo "\t<TR>\n";
	echo "\t\t<td>ชื่อ</td>\n";
	echo "\t\t<td>จำนวนที่ขอประจำ</td>\n";
	echo "\t\t<td>จำนวนที่ขอเพิ่ม</td>\n";
	echo "\t\t<td>รวม</td>\n";	
	echo "\t</TR>\n";
	
	$query = 'SELECT medicine.medicineId FROM medicine';
	
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());

	while ($line = mysql_fetch_row($result)) {
		$query2 = 'SELECT medicine.medicineName, SUM(Amount), SUM(Add_Amount) FROM medicineorder, medicine, medicinetransaction';
		$query2 = $query2.' WHERE medicineorder.Date_IDX LIKE \''.$_GET["date"].'\'';
		$query2 = $query2.' AND medicine.medicineId = medicineorder.medicineId';
		$query2 = $query2.' AND medicinetransaction.Transaction_ID = medicineorder.Transaction_ID';
		$query2 = $query2.' AND medicineorder.medicineID = \''.$line[0].'\'';
		$query2 = $query2.' AND medicinetransaction.Queue_Session = \'Chumporn\'';
		$query2 = $query2.' GROUP BY medicine.medicineName';
		
		//echo $query2."<BR>";

		$result2 = mysql_query($query2) or die('X Query failed: ' . mysql_error());

		while ($line = mysql_fetch_array($result2, MYSQL_ASSOC)) {
			echo "\t<tr>\n";
			$total = 0;
			foreach ($line as $col_value) {
				echo "\t\t<td>$col_value</td>\n";
				
				if ( is_numeric($col_value) ){
					$total = $total + $col_value;
				}
				
			}
			echo "\t\t<td>".$total."</td>\n";
			echo "\t</tr>\n";
		}

		mysql_free_result($result2);
	}

	// Free resultset
	mysql_free_result($result);

	echo "</table>\n";

	/*---------------------------- End Chumporn -------------------------------*/

	echo "<DIV STYLE='page-break-after: always;'></DIV>";

	echo "<H2>กลุ่มทหาร: ".$military_patient." คน";
	
	echo "<table border=1>\n";
	echo "\t<TR>\n";
	echo "\t\t<td>ชื่อ</td>\n";
	echo "\t\t<td>จำนวนที่ขอประจำ</td>\n";
	echo "\t\t<td>จำนวนที่ขอเพิ่ม</td>\n";
	echo "\t\t<td>รวม</td>\n";	
	echo "\t</TR>\n";
	
	$query = 'SELECT medicine.medicineId FROM medicine';
	
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());

	while ($line = mysql_fetch_row($result)) {
		$query2 = 'SELECT medicine.medicineName, SUM(Amount), SUM(Add_Amount) FROM medicineorder, medicine, medicinetransaction';
		$query2 = $query2.' WHERE medicineorder.Date_IDX LIKE \''.$_GET["date"].'\'';
		$query2 = $query2.' AND medicine.medicineId = medicineorder.medicineId';
		$query2 = $query2.' AND medicinetransaction.Transaction_ID = medicineorder.Transaction_ID';
		$query2 = $query2.' AND medicineorder.medicineID = \''.$line[0].'\'';
		$query2 = $query2.' AND medicinetransaction.Queue_Session = \'Military\'';
		$query2 = $query2.' GROUP BY medicine.medicineName';
		
		//echo $query2."<BR>";

		$result2 = mysql_query($query2) or die('X Query failed: ' . mysql_error());

		while ($line = mysql_fetch_array($result2, MYSQL_ASSOC)) {
			echo "\t<tr>\n";
			$total = 0;
			foreach ($line as $col_value) {
				echo "\t\t<td>$col_value</td>\n";
				
				if ( is_numeric($col_value) ){
					$total = $total + $col_value;
				}
				
			}
			echo "\t\t<td>".$total."</td>\n";
			echo "\t</tr>\n";
		}

		mysql_free_result($result2);
	}

	// Free resultset
	mysql_free_result($result);

	echo "</table>\n";

	/*---------------------------- End Military -------------------------------*/

	echo "<DIV STYLE='page-break-after: always;'></DIV>";

	echo "<H2>กลุ่มอาสาสมัคร1: ".$volunteer1_patient." คน";
	
	echo "<table border=1>\n";
	echo "\t<TR>\n";
	echo "\t\t<td>ชื่อ</td>\n";
	echo "\t\t<td>จำนวนที่ขอประจำ</td>\n";
	echo "\t\t<td>จำนวนที่ขอเพิ่ม</td>\n";
	echo "\t\t<td>รวม</td>\n";	
	echo "\t</TR>\n";
	
	$query = 'SELECT medicine.medicineId FROM medicine';
	
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());

	while ($line = mysql_fetch_row($result)) {
		$query2 = 'SELECT medicine.medicineName, SUM(Amount), SUM(Add_Amount) FROM medicineorder, medicine, medicinetransaction';
		$query2 = $query2.' WHERE medicineorder.Date_IDX LIKE \''.$_GET["date"].'\'';
		$query2 = $query2.' AND medicine.medicineId = medicineorder.medicineId';
		$query2 = $query2.' AND medicinetransaction.Transaction_ID = medicineorder.Transaction_ID';
		$query2 = $query2.' AND medicineorder.medicineID = \''.$line[0].'\'';
		$query2 = $query2.' AND medicinetransaction.Queue_Session = \'Volunteer1\'';
		$query2 = $query2.' GROUP BY medicine.medicineName LIMIT 0,10';
		
		//echo $query2."<BR>";

		$result2 = mysql_query($query2) or die('X Query failed: ' . mysql_error());

		while ($line = mysql_fetch_array($result2, MYSQL_ASSOC)) {
			echo "\t<tr>\n";
			$total = 0;
			foreach ($line as $col_value) {
				echo "\t\t<td>$col_value</td>\n";
				
				if ( is_numeric($col_value) ){
					$total = $total + $col_value;
				}
				
			}
			echo "\t\t<td>".$total."</td>\n";
			echo "\t</tr>\n";
		}

		mysql_free_result($result2);
	}

	// Free resultset
	mysql_free_result($result);

	echo "</table>\n";

	/*---------------------------- End Volunteer1 -------------------------------*/

	echo "<DIV STYLE='page-break-after: always;'></DIV>";

	echo "<H2>กลุ่มอาสาสมัคร2: ".$volunteer2_patient." คน";
	
	echo "<table border=1>\n";
	echo "\t<TR>\n";
	echo "\t\t<td>ชื่อ</td>\n";
	echo "\t\t<td>จำนวนที่ขอประจำ</td>\n";
	echo "\t\t<td>จำนวนที่ขอเพิ่ม</td>\n";
	echo "\t\t<td>รวม</td>\n";	
	echo "\t</TR>\n";
	
	$query = 'SELECT medicine.medicineId FROM medicine';
	
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());

	while ($line = mysql_fetch_row($result)) {
		$query2 = 'SELECT medicine.medicineName, SUM(Amount), SUM(Add_Amount) FROM medicineorder, medicine, medicinetransaction';
		$query2 = $query2.' WHERE medicineorder.Date_IDX LIKE \''.$_GET["date"].'\'';
		$query2 = $query2.' AND medicine.medicineId = medicineorder.medicineId';
		$query2 = $query2.' AND medicinetransaction.Transaction_ID = medicineorder.Transaction_ID';
		$query2 = $query2.' AND medicineorder.medicineID = \''.$line[0].'\'';
		$query2 = $query2.' AND medicinetransaction.Queue_Session = \'Volunteer2\'';
		$query2 = $query2.' GROUP BY medicine.medicineName LIMIT 0,10';
		
		//echo $query2."<BR>";

		$result2 = mysql_query($query2) or die('X Query failed: ' . mysql_error());

		while ($line = mysql_fetch_array($result2, MYSQL_ASSOC)) {
			echo "\t<tr>\n";
			$total = 0;
			foreach ($line as $col_value) {
				echo "\t\t<td>$col_value</td>\n";
				
				if ( is_numeric($col_value) ){
					$total = $total + $col_value;
				}
				
			}
			echo "\t\t<td>".$total."</td>\n";
			echo "\t</tr>\n";
		}

		mysql_free_result($result2);
	}

	// Free resultset
	mysql_free_result($result);

	echo "</table>\n";

	/*---------------------------- End Volunteer2 -------------------------------*/

	echo "<DIV STYLE='page-break-after: always;'></DIV>";

	echo "<H2>กลุ่มเช้า: ".$morning_patient." คน";
	
	echo "<table border=1>\n";
	echo "\t<TR>\n";
	echo "\t\t<td>ชื่อ</td>\n";
	echo "\t\t<td>จำนวนที่ขอประจำ</td>\n";
	echo "\t\t<td>จำนวนที่ขอเพิ่ม</td>\n";
	echo "\t\t<td>รวม</td>\n";	
	echo "\t</TR>\n";
	
	$query = 'SELECT medicine.medicineId FROM medicine';
	
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());

	while ($line = mysql_fetch_row($result)) {
		$query2 = 'SELECT medicine.medicineName, SUM(Amount), SUM(Add_Amount) FROM medicineorder, medicine, medicinetransaction';
		$query2 = $query2.' WHERE medicineorder.Date_IDX LIKE \''.$_GET["date"].'\'';
		$query2 = $query2.' AND medicine.medicineId = medicineorder.medicineId';
		$query2 = $query2.' AND medicinetransaction.Transaction_ID = medicineorder.Transaction_ID';
		$query2 = $query2.' AND medicineorder.medicineID = \''.$line[0].'\'';
		$query2 = $query2.' AND medicinetransaction.Queue_Session = \'Morning\'';
		$query2 = $query2.' GROUP BY medicine.medicineName LIMIT 0,10';
		
		//echo $query2."<BR>";

		$result2 = mysql_query($query2) or die('X Query failed: ' . mysql_error());

		while ($line = mysql_fetch_array($result2, MYSQL_ASSOC)) {
			echo "\t<tr>\n";
			$total = 0;
			foreach ($line as $col_value) {
				echo "\t\t<td>$col_value</td>\n";
				
				if ( is_numeric($col_value) ){
					$total = $total + $col_value;
				}
				
			}
			echo "\t\t<td>".$total."</td>\n";
			echo "\t</tr>\n";
		}

		mysql_free_result($result2);
	}

	// Free resultset
	mysql_free_result($result);

	echo "</table>\n";
	
	/*---------------------------- End Morning -------------------------------*/
	echo "<DIV STYLE='page-break-after: always;'></DIV>";

	echo "<H2>กลุ่มบ่าย: ".$afternoon_patient." คน";
	
	echo "<table border=1>\n";
	echo "\t<TR>\n";
	echo "\t\t<td>ชื่อ</td>\n";
	echo "\t\t<td>จำนวนที่ขอประจำ</td>\n";
	echo "\t\t<td>จำนวนที่ขอเพิ่ม</td>\n";
	echo "\t\t<td>รวม</td>\n";	
	echo "\t</TR>\n";
	
	$query = 'SELECT medicine.medicineId FROM medicine';
	
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());

	while ($line = mysql_fetch_row($result)) {
		$query2 = 'SELECT medicine.medicineName, SUM(Amount), SUM(Add_Amount) FROM medicineorder, medicine, medicinetransaction';
		$query2 = $query2.' WHERE medicineorder.Date_IDX LIKE \''.$_GET["date"].'\'';
		$query2 = $query2.' AND medicine.medicineId = medicineorder.medicineId';
		$query2 = $query2.' AND medicinetransaction.Transaction_ID = medicineorder.Transaction_ID';
		$query2 = $query2.' AND medicineorder.medicineID = \''.$line[0].'\'';
		$query2 = $query2.' AND medicinetransaction.Queue_Session = \'Afternoon\'';
		$query2 = $query2.' GROUP BY medicine.medicineName LIMIT 0,10';
		
		//echo $query2."<BR>";

		$result2 = mysql_query($query2) or die('X Query failed: ' . mysql_error());

		while ($line = mysql_fetch_array($result2, MYSQL_ASSOC)) {
			echo "\t<tr>\n";
			$total = 0;
			foreach ($line as $col_value) {
				echo "\t\t<td>$col_value</td>\n";
				
				if ( is_numeric($col_value) ){
					$total = $total + $col_value;
				}
				
			}
			echo "\t\t<td>".$total."</td>\n";
			echo "\t</tr>\n";
		}

		mysql_free_result($result2);
	}

	mysql_free_result($result);

	echo "</table>\n";

	/*---------------------------- End Afternoon -------------------------------*/
	echo "<DIV STYLE='page-break-after: always;'></DIV>";
	
	// Closing connection
	mysql_close($link);
?>
</BODY>
</HTML>