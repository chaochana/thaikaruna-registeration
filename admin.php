﻿<?php
include('config.php');
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
	<HEAD>
		<TITLE>THAI KARUNA FOUNDATION</TITLE>
		<LINK rel="stylesheet" type="text/css" href="style.css">
	</HEAD>
	<BODY>
<?php
include("header.php");
?>
	<HR>
	<BR>
	
	Main Menu || รายการหลัก 
	<UL>
		<LI><A HREF='member_management.php'>Member Management | การจัดการสมาชิก</a>
		<LI><A HREF='medicine_transaction_management.php'>Medicine Transaction Management | การจัดการระบบรับสมุนไพร</A>
		<LI><A HREF='report.php?date=<?php echo $today_IDX; ?>' TARGET="_blank">Today Medicine Transaction Report | รายงานใบสั่งสมุนไพรวันนี้</A>
		<LI><A HREF='twoweeksreport.php?date=<?php echo $today_IDX; ?>' TARGET="_blank">Today Medicine Transaction for Two Weeks Report | รายงานสมาชิกที่รับสมุนไพรสำหรับสองสัปดาห์ในวันนี้</A>		
		<LI><A HREF='anyreport.php?date=<?php echo $today_IDX; ?>' TARGET="_blank">Any Medicine Transaction Report | รายงานใบสั่งสมุนไพรวันใดๆ</A>
		<LI><A HREF='thereport.php' TARGET="_blank">Annual Report | รายงานประจำปี
		<LI><A HREF='csvreport.php?date=<?php echo $today; ?>' TARGET="_blank">CSV Report | รายงาน CSV</A>		
		<LI><A HREF='csvimport.php' TARGET="_blank">CSV Import | ข้อมูลนำเข้าแบบ CSV</A>			
	</UL>
	
	Member Menu || จัดการสมาชิก 
	<UL>
		<LI> <A HREF='member_query_result.php?type=all' target="BLANK">พิมพ์รายชื่อสมาชิกทั้งหมด</A> <A HREF='member_query_result.php?type=all&photo=yes&committee=no' target="BLANK">พิมพ์รายชื่อทั้งหมดที่มีภาพที่ไม่ใช่กรรมการ</A>
		<LI> <A HREF='member_query_result.php?type=staff' target="BLANK">พิมพ์รายชื่ออาสาสมัคร</A>
		<LI> <A HREF='member_query_result.php?type=committee' target="BLANK">พิมพ์รายชื่อกรรมการ</A>
		<LI> ส่งออกข้อมูลแบบ csv <A HREF='csvreport.php?type=all' target="BLANK">ทั้งหมด</A> <A HREF='csvreport.php?type=all-maxdate' target="BLANK">ทั้งหมดและระบุวันมาครั้งล่าสุด</A> <A HREF='csvreport.php?type=staff' target="BLANK">อาสา</A> <A HREF='csvreport.php?type=committee' target="BLANK">กรรมการ</A>
		<LI> ส่งออกข้อมูลแบบ csv สำหรับผู้ป่วยโรคต่างๆ <A HREF='csvreport_symptom.php?symptom=หัวใจ' target="BLANK">หัวใจ</A>  <A HREF='csvreport_symptom.php?symptom=เบาหวาน' target="BLANK">เบาหวาน</A>  <A HREF='csvreport_symptom.php?symptom=ความดัน' target="BLANK">ความดัน</A>  <A HREF='csvreport_symptom.php?symptom=มะเร็ง' target="BLANK">มะเร็ง</A>  <A HREF='csvreport_symptom.php?symptom=เก๊า' target="BLANK">เก๊า</A>
		<LI> ส่งออกข้อมูลแบบ csv สำหรับผู้สมัครในช่วงเวลาต่างๆ <A HREF='csvreport_apply.php?apply=2011-01' target="BLANK">มกราคม 2554</A> <A HREF='csvreport_apply.php?apply=2011-02' target="BLANK">กุมภาพันธ์ 2554</A>  <A HREF='csvreport_apply.php?apply=2011-03' target="BLANK">มีนาคม 2554</A>  <A HREF='csvreport_apply.php?apply=2011-04' target="BLANK">เมษายน 2554</A> 
    </UL>
	<!--
	<script language="JavaScript"> 
		alert("ประกาศา\n1. เครื่อง server ย้ายจาก 192.168.2.102 เป็น 192.168.2.109 สามารถเข้าสู่ระบบจัดการได้ทาง http://192.168.2.109/thaikrn\n2. ท่านใดพบความผิดปกติของภาษา โดยถ้ามีบางส่วนของเว็บแสดงภาษาไทยเป็น ???? รบกวนแจ้ง เชา ด้วย\n\n* ประกาศนี้แสดงวันนี้วันเดียว เดี๋ยวจะเอาออกนะครับ");
	</script>  
	-->
	</BODY>
</HTML>